import numpy as np
import pylab as pl
from ase.units import Hartree, kcal, mol
from ase.io import read
from ase_interface_mpi import SCME_PS
from ase.optimize import BFGS
from ase.io.trajectory import Trajectory as Tr
from ase.constraints import FixAtoms
#from ase.optimize.bfgslinesearch import BFGSLineSearch as BFGS

from pylab import rcParams
rcParams['figure.figsize'] = 10, 7

# Pentamers
pentamers = ['CYC','FRB','CAC',
             'CAA','CAB','FRC',
             'FRA']

energy_pentamers = np.array([-381.910877, -381.908608, -381.908032,
                             -381.907815, -381.906536, -381.904564,
                             -381.905669]) * Hartree

corr_pentamers = np.array([0.37, 0.08, -0.10,
                           -0.08, -0.17, -0.02,
                           -0.02]) * kcal / mol

# Hexamers
hexamers = ['prism', 'cage', 'book-1', 
            'book-2','bag','cyclic-ring',
            'cyclic-boat-1','cyclic-boat-2']

energy_hexamers = np.array([-458.296817, -458.296753, -458.296426, 
                            -458.295839, -458.295335, -458.294962,
                            -458.293693, -458.293581]) * Hartree

corr_hexamers = np.array([-0.13, 0.05, 0.26, 
                           0.26, 0.48, 0.26, 
                           0.49, 0.47]) * kcal / mol

#fact_a=0.448004523
#fact_b=1.36954266
te=1.10036499e+00
td=7.09887127e+00
AO=8.66033865e+03
bO=-5.64462066e-01
cO=-3.48562837e+00

# SCME pentamers
pentamers_fixed = np.zeros_like(energy_pentamers)
pentamers_opt   = np.zeros_like(energy_pentamers)

for i, a in enumerate(pentamers):
    cluster = read('pentamers/'+a+'.xyz')
    if a == 'CAB':
        c = FixAtoms(indices=[atom.index for atom in cluster if atom.symbol == 'O'])
        cluster.set_constraint(c)
    cluster.cell = (35,35,35)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)

    pentamers_fixed[i] = cluster.get_potential_energy()
    
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj', maxstep=0.01)
    dyn.run(fmax=0.001)
    pentamers_opt[i] = cluster.get_potential_energy()


energy_pentamers += corr_pentamers
# Energy differences - hexamers
dE_cbs  = (energy_pentamers - energy_pentamers[0])
dE_fixed = (pentamers_fixed - pentamers_fixed[0])
dE_opt = (pentamers_opt - pentamers_opt[0])

x = np.arange(0,len(pentamers),1)

# Read in SCME rigid energies
data = open('energies.data', 'r')
e = []
lines = data.readlines()

for a in [8,9,10,11,12,13,14]:
    e.append(float(lines[a].split()[0]))

e = np.array(e)
e -= e[0]

pl.plot(x, dE_cbs * mol / kcal, 'k*',markersize=12)
pl.plot(x, dE_cbs * mol / kcal, 'k--',linewidth=3, label='CCSD(T)')
pl.plot(x, dE_opt * mol / kcal, 'r*',markersize=12)
pl.plot(x, dE_opt * mol / kcal, 'r--',linewidth=3, label='f-SCME')
pl.plot(x, e * mol / kcal, 'g*',markersize=12)
pl.plot(x, e * mol / kcal, 'g--',linewidth=3, label='SCME')
pl.legend(loc='lower right',fontsize=14)
pl.ylabel(r'$\Delta$E [kcal/mol]', fontsize=16)
pl.title('Pentamers',fontsize=18)
pl.xticks([0,1,2,3,4,5,6],pentamers,fontsize=14)
pl.yticks(fontsize=14)
pl.ylim([0, 4.5])
pl.xlim([-0.5,6.5])

import matplotlib
fig = matplotlib.pyplot.gcf()
fig.savefig('test.pdf', dpi=180)

pl.show()



# SCME hexamers
hexamers_fixed = np.zeros_like(energy_hexamers)
hexamers_opt   = np.zeros_like(energy_hexamers)

for i, a in enumerate(hexamers):
    cluster = read('hexamers/'+a+'.xyz')
    cluster.cell = (35,35,35)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)

    hexamers_fixed[i] = cluster.get_potential_energy()
    
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj')
    dyn.run(fmax=0.001)
    hexamers_opt[i] = cluster.get_potential_energy()


energy_hexamers += corr_hexamers
# Energy differences - hexamers
dE_cbs  = (energy_hexamers - energy_hexamers[0])
dE_fixed = (hexamers_fixed - hexamers_fixed[0])
dE_opt = (hexamers_opt - hexamers_opt[0])

x = np.arange(0,len(hexamers),1)

e = []

for a in [16,17,18,19,20,21,22,23]:
    e.append(float(lines[a].split()[0]))

e = np.array(e)
e -= e[0]

pl.plot(x, dE_cbs * mol / kcal, 'k*', markersize=12)
pl.plot(x, dE_cbs * mol / kcal, 'k--', linewidth=3, label='CCSD(T)')
pl.plot(x, dE_opt * mol / kcal, 'r*', markersize=12)
pl.plot(x, dE_opt * mol / kcal, 'r--', linewidth=3, label='f-SCME')
pl.plot(x, e * mol / kcal, 'g*', markersize=12)
pl.plot(x, e * mol / kcal, 'g--', linewidth=3, label='SCME')
pl.legend(loc='lower right', fontsize=14)
pl.ylabel(r'$\Delta$E [kcal/mol]', fontsize=16)
pl.title('Hexamers', fontsize=18)
pl.xticks([0,1,2,3,4,5,6,7],['PRI','CAG','BK1','BK2','BAG','CYR','CB1','CB2'],fontsize=14)
pl.yticks(fontsize=14)
pl.xlim([-0.5, 7.5])
pl.ylim([-1, 3.5])
pl.show()
