from asap3.analysis.rdf import RadialDistributionFunction as RDF
from ase.io.trajectory import Trajectory as Tr

traj = Tr('tot.traj')[3000:]
RDFobj = None

rcut = 8.0
nbins = 200

for atoms in traj:
    if RDFobj is None:
        RDFobj = RDF(atoms[::3], rcut, nbins)
    else:
        RDFobj.atoms = atoms[::3]
    RDFobj.update()


from ase.data import atomic_numbers
H = atomic_numbers['H']
O = atomic_numbers['H']

#rdf = RDFobj.get_rdf(elements=(H, O))
rdf = RDFobj.get_rdf()

import numpy as np
import pylab as pl


# Read in SCMEOLD data
data = open('all_01_Omm-Omm.dat', 'r')
lines = data.readlines()

roo = []
r   = []

for i, line in enumerate(lines):
    r.append(float(line.split()[0]))
    roo.append(float(line.split()[1]))

r = np.array(r)
roo = np.array(roo)

x = (np.arange(nbins) + 0.5) * rcut / nbins

pl.plot(x, rdf)
pl.plot(r, roo)
pl.ylim([0,3.5])
pl.xlim([2,8.0])
pl.show()
