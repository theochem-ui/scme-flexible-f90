import numpy as np
import pylab as pl
from ase.io.trajectory import Trajectory as Tr
from ase.io import read
import sys

print('Transform to methyl group!!!')
xxx

# List of arguments
arg = int(sys.argv[1])

aceSAPT = open('bukowski1999supp1a.txt', 'r')
aceSAPT_l = aceSAPT.readlines()
aceSAPT_t = aceSAPT_l.copy()

for i, line in enumerate(aceSAPT_l):
    aceSAPT_t.append(line)

print(aceSAPT_l)
# idx - argument 1
disp = []
for i, line in enumerate(aceSAPT_t):
    disp.append(float(line.split()[arg]))

from ase.units import kcal, mol
from ase.visualize import view

print(np.array(disp)*kcal/mol)

from ase_interface_mpi import SCME_PS

def get_scme(p):
    # Zeroise
    calc_disp = np.zeros(len(disp))

    for i in np.arange(1,len(disp)+1):
        if i < 10:
            a = '00%d' %i
        elif 10 <= i < 100:
            a = '0%d' %i
        else:
            a = i

        ace_dimer = read('coord_%s.xyz' %a)
        ace_dimer.center(vacuum=13.)

        view(ace_dimer)

        ace_dimer.calc = SCME_PS(ace_dimer,
                            #fact_a=p[0],
                            #fact_b=p[1],
                            #fact_c=p[2],
                            #f_req=p[3],
                            #t_req=p[4],
                            #f_ang=p[5],
                            #t_ang=p[6]
                            )

        ace_dimer.get_potential_energy()

        calc_disp[i] = ace_dimer.calc.uDisp

    return calc_disp

#get_scme(np.zeros(7))

def func(p):
    x = get_scme(p)
    print(np.sqrt((x - disp)**2)/len(x))
    return x - disp

from scipy.optimize import minimize, least_squares

bounds = np.array([[-8.0, 8.0],
                   [-8.0, 8.0],
                   [-8.0, 8.0],
                   [-2.0, 2.0],
                   [0.0, 2.0],
                   [-1.0, 1.0],
                   [0.0, 2.0]])

initial_guess =  np.array([0.8915508, 0.7954219, -0.6631, 
                              -1.5160886, 0.22219494, -0.2500541, 0.90272683]), 

res = least_squares(func,
                    initial_guess,
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf',
                    ftol=1e-10, gtol=1e-10, xtol=1e-10,
                    max_nfev=100, # Number of itr. 
                    verbose=1)
print(res.x)
