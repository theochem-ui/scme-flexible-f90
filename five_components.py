import numpy as np
import pylab as pl
from ase.io.trajectory import Trajectory as Tr

quad_ccsd = open('../make_water/quadrupole_out_total.data', 'r')
qccsd_l = quad_ccsd.readlines()
dip_ccsd = open('scme_dipole_focus.data', 'r')
dccsd_l = dip_ccsd.readlines()

qccsd = np.zeros((len(qccsd_l),6))
qscme = np.zeros_like(qccsd)

dR = np.zeros(len(qccsd_l))

for i, line in enumerate(qccsd_l):
    qccsd[i,0] = float(qccsd_l[i].split()[3])
    qccsd[i,1] = float(qccsd_l[i].split()[4])
    qccsd[i,2] = float(qccsd_l[i].split()[5])
    qccsd[i,3] = float(qccsd_l[i].split()[6])
    qccsd[i,4] = float(qccsd_l[i].split()[7])
    qccsd[i,5] = float(qccsd_l[i].split()[8])

    #dR[i] = float(dccsd_l[i].split()[6])

from ase.units import Bohr, Debye
qccsd *= Bohr**2 / Debye * 3. / 2.

for i, qc in enumerate(qccsd):
    qt = 1./3. * (qc[0] + qc[1] + qc[2])
    qc[0] -= qt
    qc[1] -= qt
    qc[2] -= qt

# Read in geometries
geos = open('../new_make_water/water_new.data', 'r')
geo_lines = geos.readlines()

traj = Tr('h2o.traj')

from ase_interface_mpi import SCME_PS

def get_quad_scme(p):
    # Zeroise
    qscme[:,:3] = 0.0

    for i, line in enumerate(geo_lines):
        h2o = traj[-1]
        ang = float(line.split()[0])
        r1  = float(line.split()[1])
        r2  = float(line.split()[2])

        #
        O = h2o[0:1].copy()
        O.positions[0,2] -= 1.0

        h2oD = h2o + O

        h2oD.set_angle(3,0,1,ang/2.)
        h2oD.set_angle(3,0,2,ang/2.)

        h2oD.set_distance(0,1,r1,fix=0)
        h2oD.set_distance(0,2,r2,fix=0)

        del h2oD[3]

        h2oD.calc = SCME_PS(h2oD,
                            fact_a=p[0],
                            fact_b=p[1],
                            fact_c=p[2],
                            f_req=p[3],
                            t_req=p[4],
                            f_ang=p[5],
                            t_ang=p[6]
                            )

        h2oD.get_potential_energy()

        quad = h2oD.calc.quad[0]

        qscme[i,0] = quad[0,0]
        qscme[i,1] = quad[1,1]
        qscme[i,2] = quad[2,2]

    return qscme, qccsd

def func(p):
    x, y = get_quad_scme(p)

    dQt = np.zeros(len(y))
    for j, q in enumerate(x):
        Qts = q[0] + q[2] / 2.
        Qtc = y[j][0] + y[j][2] / 2.
        dQt[j] = Qts - Qtc

    #G = dR <= 0.20

    print(np.sqrt((dQt)**2)/np.sqrt(Qts**2) * 100.)

    return dQt

from scipy.optimize import minimize, least_squares

bounds = np.array([[-8.0, 8.0],
                   [-8.0, 8.0],
                   [-8.0, 8.0],
                   [-2.0, 2.0],
                   [0.0, 2.0],
                   [-1.0, 1.0],
                   [0.0, 2.0]])

res = least_squares(func, 
                    np.array([0.5, 0.5, 0.5, 0.25, 1.0, -0.25, 1.0]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=1e-10, xtol=1e-10,                                                              max_nfev=100, verbose=1)
print(res.x)
