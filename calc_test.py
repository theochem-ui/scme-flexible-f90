import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS

energy = []
volume = []

#[ 8.73726492e-01  4.99991449e+00  1.62360435e+04 -1.99998255e+00
# -3.05503982e+00]


a = ['0.856000','0.880000','0.904000',
     '0.928000','0.952000','0.976000',
     '1.000000','1.024000','1.048000',
     '1.072000']

#a = ['0.992000','1.008000','1.024000','1.040000']

#te=1.13194122e+00
#td=6.40424458e+00
#Ar=8.67162778e+03
#br=-6.56626386e-01
#cr=-3.47132531e+00

# BEST SO FAR
#te=1.10454150e+00
#td=7.55480010e+00
#Ar=8.14963246e+03
#br=-5.51527551e-01
#cr=-3.46949797e+00

# LAST ?
#te=1.18989733e+00  
#td=5.35942255e+00  
#Ar=5.00004561e+03 
#br=-3.68373639e+00
#cr=-2.21523360e+00

#te=1.09442228e+00  
#td=7.56424049e+00  
#Ar=8.71032621e+03 
#br=-5.27481541e-01
#cr=-3.50213393e+00

te=1.09984359e+00
td=7.61857402e+00
Ar=8.57778344e+03
br=-5.34224485e-01
cr=-3.50375235e+00

# Read in all relaxed ice-Ih structures
# Relax from new parameters
for val in a:
    traj = Tr('data/ice-scme-%s-new.traj' %val)
    ice = traj[-1]

    #ice = sys.repeat((2,2,2))
    cell = ice.cell.diagonal()
    vol = cell[0]*cell[1]*cell[2]

    ice.calc = SCME_PS(ice, 
                   #numerical=True,
                   #repeat=(2,2,2),
                   fix_oxygen=False,
                   te=te,
                   td=td,
                   AO=Ar,
                   bO=br,
                   cO=cr,
                   #parallel=True,
                   static_flag='dqoh', # <--- FLAG to turn on/off static moments
                   ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                   flex_flag='dq')     # <--- FLAG to turn on/off flexible moments

    #dyn = BFGS(ice, trajectory='data/ice-qha-%s-newpara.traj' %val)
    #dyn.run(fmax=0.01)

    e = ice.get_potential_energy()
    print(e)
    energy.append(e/8./96.)
    volume.append(vol/8./96.)

energy = np.array(energy)
volume = np.array(volume)

def adjust(energy, volume, index, de):
    # index marks zero : 4
    a = de / volume[index+1]
    energy[index+1:] -= a * volume[index+1:]

    return energy

#energy = adjust(energy,volume,4,0.0007)

#energy = adjust(energy,volume,5,0.001)
#energy = adjust(energy,volume,6,0.002)

print(energy)

data = open('e-v-2.dat')
lines = data.readlines()

volume_o = []
energy_o = []

for line in lines:
    energy_o.append(float(line.split()[1]))
    volume_o.append(float(line.split()[0]))

energy_o = np.array(energy_o)
volume_o = np.array(volume_o)

from ase.units import kJ
from ase.eos import EquationOfState

eos = EquationOfState(volume, energy)
v0n, e0n, Bn = eos.fit()
print(Bn / kJ * 1.0e24, 'GPa, NEW')
print(v0n, 'Volume, NEW')
print(e0n, 'Coh. NEW')

eos = EquationOfState(volume_o/96.0, energy_o/96.0)
v0n, e0n, Bn = eos.fit()
print(Bn / kJ * 1.0e24, 'GPa, OLD')
print(v0n, 'Volume, OLD')
print(e0n, 'Coh. OLD')

import pylab as pl
pl.plot(volume, energy - np.min(energy), 'b--', label='NEW')
pl.plot(volume_o/96.0, energy_o/96.0 - np.min(energy_o/96.0), 'r--', label='OLD')
pl.legend(loc='best')
pl.show()
