from ase import Atoms
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase_interface_mpi import SCME_PS
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
import numpy as np

te=1.10036499e+00
td=7.09887127e+00
AO=8.66033865e+03
bO=-5.64462066e-01
cO=-3.48562837e+00

# Read in previous from tip3p
traj = Trajectory('fSCME_729mol_equil-T.traj')
atoms = traj[-1]

# Equilibrate - 729 molecules
tag = 'fSCME_729mol_equil-U'
atoms.set_constraint()  # remove constraints

atoms.calc = SCME_PS(atoms,
                     numerical=False,
                     te=te,
                     td=td,
                     AO=AO,
                     bO=bO,
                     cO=cO)

md = Langevin(atoms, 0.5 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '.log')

traj = Trajectory(tag + '.traj', 'w', atoms)
md.attach(traj.write, interval=5)
md.run(10000)
