!----------------------------------------------------------------------+
!     Routine to fix molecules broken due to the periodic boundary     |
!     conditions                                                       |
!----------------------------------------------------------------------+

module molecProperties

  use data_types
  use max_parameters
  use tang_toennies, only: Tang_ToenniesN, Tang_ToenniesNdF

  private
  public recoverMolecules, calcCentersOfMass, findPpalAxes, rotatePoles,&
       rotatePolariz, setUnpolPoles, addFields, addDfields, SF, SFdsf,&
       bisectorAxes, stonedampingNF, atomicForces, atomicFFlexible,&
       dummyAtoms

contains

  subroutine recoverMolecules(raOri, ra, nH, nO, a, a2)

    implicit real(dp) (a-h,o-z)

    real(dp) raOri(maxCoo), a(3), a2(3), ra(maxCoo), dist
    integer nH,nO

    do i = 1, nO
       do l = 1, 3
          do n = 1, 2
             index = l+3*(n-1)+6*(i-1)
             dist = raOri(index) - raOri(l+3*(i-1+nH))
             if (dist .gt. a2(l)) then
                ra(index) = raOri(index) - a(l)
             elseif(dist .lt. -a2(l)) then
                ra(index) = raOri(index) + a(l)
             else
                ra(index) = raOri(index)
             end if
          end do
          ra(l+3*(i-1+nH)) = raOri(l+3*(i-1+nH))
       end do
    end do
    return

  end subroutine recoverMolecules

  !----------------------------------------------------------------------+
  !     Routine to calculate the center of mass of each molecule         |
  !----------------------------------------------------------------------+
  subroutine calcCentersOfMass(ra, nM, rCM)

    implicit real(dp) (a-h,o-z)
    integer nM, iH1, iH2, iO, i, j
    real(dp) rCM(3,maxCoo/3), ra(maxCoo)

    do i = 1, nM
       iH2 = 2 * i
       iH1 = iH2 - 1
       iO  = 2 * nM + i

       iH1 = 3 * (iH1 - 1)
       iH2 = 3 * (iH2 - 1)
       iO  = 3 * (iO - 1)
       do j = 1, 3
          rCM(j,i) = (ra(iH1+j) + ra(iH2+j) + 16.d0 * ra(iO+j)) / 18.d0
       end do
    end do

    return

  end subroutine calcCentersOfMass

  !----------------------------------------------------------------------+
  !     Routine to calculate the principal axes of each molecule         |
  !----------------------------------------------------------------------+
  subroutine findPpalAxesOLD(ra, nM, x)

    implicit real(dp) (a-h,o-z)

    integer nM, iH1, iH2, iO, i, j
    real(dp) x(3,3,maxCoo/3), ra(maxCoo), r11, r21

    do i = 1, nM
       iH2 = 2 * i
       iH1 = iH2 - 1
       iO  = 2 * nM + i

       iH1 = 3 * (iH1 - 1)
       iH2 = 3 * (iH2 - 1)
       iO  = 3 * (iO - 1)
       do j = 1, 3
          x(j,1,i) = -(ra(iH1+j) + ra(iH2+j) - 2.d0 * ra(iO+j))
          x(j,2,i) = ra(iH2+j) - ra(iH1+j)
       end do
       r11 = sqrt(x(1,1,i)*x(1,1,i) + x(2,1,i)*x(2,1,i) + x(3,1,i) * x(3,1,i))
       r21 = sqrt(x(1,2,i)*x(1,2,i) + x(2,2,i)*x(2,2,i) + x(3,2,i) * x(3,2,i))
       do j = 1, 3
          x(j,1,i) = x(j,1,i) / r11
          x(j,2,i) = x(j,2,i) / r21
       end do
       x(1,3,i) = x(2,1,i) * x(3,2,i) - x(3,1,i) * x(2,2,i)
       x(2,3,i) = x(3,1,i) * x(1,2,i) - x(1,1,i) * x(3,2,i)
       x(3,3,i) = x(1,1,i) * x(2,2,i) - x(2,1,i) * x(1,2,i)
    end do
    return

  end subroutine findPpalAxesOLD

  !----------------------------------------------------------------------+
  !     Routine to calculate the principal axes of each molecule         |
  !----------------------------------------------------------------------+
  subroutine findPpalAxes(ra, nM, x)

    implicit real(dp) (a-h,o-z)

    integer nM, iH1, iH2, iO, i, j
    real(dp) x(3,3,maxCoo/3), ra(maxCoo), r11, r21

    ! Debug
    integer*4 p

    do i = 1, nM
       iH2 = 2 * i
       iH1 = iH2 - 1
       iO  = 2 * nM + i

       iH1 = 3 * (iH1 - 1)
       iH2 = 3 * (iH2 - 1)
       iO  = 3 * (iO - 1)
       do j = 1, 3
          x(j,3,i) = -(ra(iH1+j) + ra(iH2+j) - 2.d0 * ra(iO+j))
          x(j,1,i) = ra(iH2+j) - ra(iH1+j)
       end do
       r11 = sqrt(x(1,3,i)*x(1,3,i) + x(2,3,i)*x(2,3,i) + x(3,3,i) * x(3,3,i))
       r21 = sqrt(x(1,1,i)*x(1,1,i) + x(2,1,i)*x(2,1,i) + x(3,1,i) * x(3,1,i))

       do j = 1, 3
          x(j,3,i) = x(j,3,i) / r11
          x(j,1,i) = x(j,1,i) / r21
       end do
       x(1,2,i) = x(2,3,i) * x(3,1,i) - x(3,3,i) * x(2,1,i)
       x(2,2,i) = x(3,3,i) * x(1,1,i) - x(1,3,i) * x(3,1,i)
       x(3,2,i) = x(1,3,i) * x(2,1,i) - x(2,3,i) * x(1,1,i)
    end do

    return

  end subroutine findPpalAxes


  !----------------------------------------------------------------------+
  !> Routine to rotate the multipoles to the orientation of the molecules.
  !----------------------------------------------------------------------+
  subroutine rotatePoles(d0, q0, o0, h0, dpole, qpole, opole, hpole, nM, x)
    implicit none
    real(dp), intent(in) :: d0(3)
    real(dp), intent(in) :: q0(3,3)
    real(dp), intent(in) :: o0(3,3,3)
    real(dp), intent(in) :: h0(3,3,3,3)
    real(dp), intent(out) :: dpole(3,nM)
    real(dp), intent(out) :: qpole(3,3,nM)
    real(dp), intent(out) :: opole(3,3,3,nM)
    real(dp), intent(out) :: hpole(3,3,3,3,nM)
    integer, intent(in) :: nM
    real(dp), intent(in) :: x(3,3,nM)
    ! ----------------------------------------

    ! local variables.
    integer i, j, k, l, ii, jj, kk, ll, m

    ! Zeroise.
    hpole(1:3, 1:3, 1:3, 1:3, 1:nM) = 0.0d0
    opole(1:3, 1:3, 1:3, 1:nM) = 0.0d0
    qpole(1:3, 1:3, 1:nM) = 0.0d0
    dpole(1:3, 1:nM) = 0.0d0

    ! Do the calculation.
    do m = 1, nM
       do l = 1, 3
          do ll = 1, 3
             do k = 1, 3
                do kk = 1, 3
                   do j = 1, 3
                      do jj = 1, 3
                         do i = 1, 3
                            do ii = 1, 3
                               hpole(i,j,k,l,m) = hpole(i,j,k,l,m) + &
                                    x(i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    * x(l,ll,m) * h0(ii,jj,kk,ll)
                            end do
                         end do
                         opole(j,k,l,m) = opole(j,k,l,m) + &
                              x(j,jj,m)* x(k,kk,m)* x(l,ll,m) * o0(jj,kk,ll)
                      end do
                   end do
                   qpole(k,l,m) = qpole(k,l,m) + x(k,kk,m) * x(l,ll,m)* q0(kk,ll)
                end do
             end do
             dpole(l,m) = dpole(l,m) + x(l,ll,m) * d0(ll)
          end do
       end do
    end do

    return

  end subroutine rotatePoles

  !----------------------------------------------------------------------+
  !     Routine to rotate the multipoles to the orientation of the       |
  !     molecule                                                         |
  !----------------------------------------------------------------------+
  subroutine setUnpolPoles(dpole, qpole, dpole0, qpole0, nM)

    implicit none

    integer nM, i, j, k
    real(dp) dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
    real(dp) dpole0(3,maxCoo/3), qpole0(3,3,maxCoo/3)

    do i = 1, nM
       do j = 1, 3
          dPole(j,i) = dpole0(j,i)
          do k = 1, 3
             qpole(k,j,i) = qpole0(k,j,i)
          end do
       end do
    end do
    return

  end subroutine setUnpolPoles

  !----------------------------------------------------------------------+
  !> Rotate polarization matrices to the global reference frame
  !----------------------------------------------------------------------+
  subroutine rotatePolariz(dd0, dq0, qq0, hp0, dd, dq, qq, hp, nM, x)

    implicit none
    real(dp), intent(in) :: hp0(3,3,3)
    real(dp), intent(in) :: dq0(3,3,3)
    real(dp), intent(in) :: dd0(3,3)
    real(dp), intent(in) :: qq0(3,3,3,3)
    real(dp), intent(out) :: hp(3,3,3,nM)
    real(dp), intent(out) :: dq(3,3,3,nM)
    real(dp), intent(out) :: dd(3,3,nM)
    real(dp), intent(out) :: qq(3,3,3,3,nM)
    integer, intent(in) :: nM
    real(dp), intent(in) :: x(3,3,nM)
    ! ----------------------------------------

    ! Local variables.
    integer i, j, k, l, ii, jj, kk, ll, m

    ! Zeroise.
    dd(1:3, 1:3, 1:nM) = 0.d0
    dq(1:3, 1:3, 1:3, 1:nM) = 0.d0
    qq(1:3, 1:3, 1:3, 1:3, 1:nM) = 0.d0
    hp(1:3, 1:3, 1:3, 1:nM) = 0.d0

    ! Do the calculation.
    do m = 1, nM
       do k = 1, 3
          do kk = 1, 3
             do j = 1, 3
                do jj = 1, 3
                   do i = 1, 3
                      do ii = 1, 3
                         do l = 1, 3
                            do ll = 1, 3
                               qq(l,i,j,k,m) = qq(l,i,j,k,m) + &
                                    x(l,ll,m) *  x(i,ii,m)*x(j,jj,m)&
                                    * x(k,kk,m) * qq0(ll,ii,jj,kk)
                            end do
                         end do
                         dq(i,j,k,m) = dq(i,j,k,m) + x(i,ii,m) * x(j,jj,m) &
                              * x(k,kk,m) * dq0(ii,jj,kk)
                         hp(i,j,k,m) = hp(i,j,k,m) + x(i,ii,m) * x(j,jj,m) &
                              * x(k,kk,m) * hp0(ii,jj,kk)
                      end do
                   end do
                   dd(j,k,m) = dd(j,k,m) + x(j,jj,m) * x(k,kk,m)* dd0(jj,kk)
                end do
             end do
          end do
       end do
    end do

    return

  end subroutine rotatePolariz

  !----------------------------------------------------------------------+
  !     Add all the fields                                               |
  !----------------------------------------------------------------------+
  subroutine addFields(eH, eD, eT, nM)

    implicit none
    integer i, j, nM
    real(dp) eH(3,maxCoo/3), eD(3,maxCoo/3)
    real(dp) eT(3,maxCoo/3)

    do i = 1, nM
       do j = 1, 3
          eT(j,i) = eH(j,i) + eD(j,i)
       end do
    end do
    return

  end subroutine addFields

  !--------------------------------------------------------------------+
  !     Add the derivative of all the fields                           |
  !--------------------------------------------------------------------+
  subroutine addDfields(dEhdr, dEddr, dEtdr, nM)

    implicit none
    integer i, j, k, nM
    real(dp) dEhdr(3,3,maxCoo/3)
    real(dp) dEtdr(3,3,maxCoo/3), dEddr(3,3,maxCoo/3)

    do i = 1, nM
       do j = 1, 3
          do k = 1, 3
             dEtdr(k,j,i) = dEhdr(k,j,i) + dEddr(k,j,i)
          end do
       end do
       do j = 2, 3
          do k = 1, j-1
             dEtdr(j,k,i) = dEtdr(k,j,i)
          end do
       end do
    end do
    return

  end subroutine addDfields

  !-------------------------------------------------------------------+
  ! Switching function - no derivative                                !
  !-------------------------------------------------------------------+
  subroutine SF(r, swFunc)

    implicit none
    real(dp) r, swFunc, x, x2, x3, rSW, rCut
    real(dp) rL1, rL2, rH1, rH2, dr

    data rL1, rH1, rL2, rH2 / 0.d0, 5.d0, 9.d0, 11.d0 /
    save

    if (r .le. rL2) then
       swFunc = 1.0d0
    else if (r .ge. rH2) then
       swFunc = 0.0d0
    else
       x = (r - rL2)/(rH2 - rL2)
       x2 = x * x
       x3 = x2 * x
       swFunc = 1.d0 + x3 * (-6.d0 * x2 + 15.d0 * x - 10.d0)
    end if

    return

  end subroutine SF

  !--------------------------------------------------------------------+
  ! Switching function - with derivative of long range cut-off         !
  !--------------------------------------------------------------------+
  subroutine SFdsf(r, swFunc, dSdr)

    implicit none
    real(dp) r, swFunc, x, x2, x3, dSdr, rSW, rCut
    real(dp) rL1, rL2, rH1, rH2, dr

    data rL1, rH1, rL2, rH2 / 0.d0, 5.d0, 9.d0, 11.d0 /
    save


    if (r .le. rL2) then
       swFunc = 1.0d0
       dSdr   = 0.0d0
    else if (r .ge. rH2) then
       swFunc = 0.0d0
       dSdr   = 0.0d0
    else
       x = (r - rL2)/(rH2 - rL2)
       dr = 1.d0 / (rH2 - rL2)
       x2 = x * x
       x3 = x2 * x
       swFunc = 1.d0 + x3 * (-6.d0 * x2 + 15.d0 * x - 10.d0)
       dSdr = 30.d0 * x2 * (- x2 + 2.d0 * x - 1.d0) * dr
    end if

    return

  end subroutine SFdsf

  !--------------------------------------------------------------------+
  ! Stones electrostatic interaction damping functions                 !
  !--------------------------------------------------------------------+
  subroutine stonedampingNF(r, d, m, te)         
  
    implicit none                                      
    real(dp) r, d, te
    integer n, m, i
  
    real(dp) S, fact, other, pi 
                                           
    pi = 3.14159265358979324d0             
                                           
    S = r / te
    d = erf(S)                             
    fact = 1.d0                            
    other = 2.d0 / sqrt(pi) * exp(-S * S)
    n = (m - 1) / 2
  
    do i = 1, n            
       if (i .gt. 1) then                  
          fact = fact * (2 * (i - 1) + 1)  
       end if                            
       d = d - S**(2 * (i - 1) + 1) * 2.d0**(i - 1) / fact * other
    end do 
  
  end subroutine stonedampingNF  

  !---------------------------------------------------------------------+
  !  Calculate bisector axes, rotation matrix, and derivatives thereof  !
  !---------------------------------------------------------------------+
  subroutine bisectorAxes(ra, nM, rot, dx, dxx, dxz, rCM)

    real(dp) rot(3,3,maxCoo/3), ra(maxCoo), rCM(3,maxCoo/3)
    real(dp) dx(3,3,3,maxCoo/3), dxx(3,3,3,maxCoo/3), dxz(3,3,3,maxCoo/3)
    integer nM, iH1, iH2, i, j, k, l
    real(dp) V(3), C(3), U(3), X(3) 
    real(dp) Vn, Cn, Un, Vez, Xn
    real(dp) ez(3), ex(3), ey(3) ! unit vectors defining rotation

    real(dp) dzdu(3,3), dxdv(3,3), dxdz(3,3), dydz(3,3), dydx(3,3) ! Derivatives
    real(dp) dudri(3,3), dudriz(3,3), dudrix(3,3), dvdri(3,3), dvdrix(3,3)
    real(dp) ezi(3,3), eziz(3,3), ezix(3,3), exi(3,3), exiz(3,3), exix(3,3), &
            eyi(3,3), eyiz(3,3), eyix(3,3)

    do i = 1,nM
       iH1 = 6 * (i - 1)
       iH2 = 3 + 6 * (i -1)

       do j = 1,3
          V(j) = rCM(j,i) - ra(iH1 + j)
          C(j) = rCM(j,i) - ra(iH2 + j)
       end do

       Vn  = sqrt(V(1)**2 + V(2)**2 + V(3)**2)
       Cn  = sqrt(C(1)**2 + C(2)**2 + C(3)**2)

       do j = 1,3
          U(j) = Vn*C(j) + Cn*V(j)
       end do

       Un = sqrt(U(1)**2 + U(2)**2 + U(3)**2)

       do j = 1,3
          ez(j) = U(j) / Un
       end do

       Vez = ez(1)*V(1) + ez(2)*V(2) + ez(3)*V(3)

       do j = 1,3
          X(j) = V(j) - Vez*ez(j)
       end do

       Xn = sqrt(X(1)**2 + X(2)**2 + X(3)**2)

       do j = 1,3
          ex(j) = X(j) / Xn
       end do

       ey(1) = ez(2)*ex(3) - ez(3)*ex(2)
       ey(2) = ez(3)*ex(1) - ez(1)*ex(3)
       ey(3) = ez(1)*ex(2) - ez(2)*ex(1)

       do j = 1,3
          rot(j,1,i) = ex(j)
          rot(j,2,i) = ey(j)
          rot(j,3,i) = ez(j)
       end do

       ! Zeroise
       dzdu(:,:) = 0.d0
       dxdv(:,:) = 0.d0
       dxdz(:,:) = 0.d0
       dydz(:,:) = 0.d0
       dydx(:,:) = 0.d0

       dudri(:,:)  = 0.d0
       dudrix(:,:) = 0.d0
       dudriz(:,:) = 0.d0
       dvdri(:,:)  = 0.d0
       dvdrix(:,:) = 0.d0

       ezi(:,:)  = 0.d0
       eziz(:,:) = 0.d0
       ezix(:,:) = 0.d0

       exi(:,:)  = 0.d0
       exiz(:,:) = 0.d0
       exix(:,:) = 0.d0

       eyi(:,:)  = 0.d0
       eyiz(:,:) = 0.d0
       eyix(:,:) = 0.d0

       ! Derivatives - vector/vector
       Un3 = Un*Un*Un
       Xn2 = Xn*Xn

       do j = 1, 3
          dzdu(j,j) = 1.d0 / Un
          dxdv(j,j) = 1.d0 / Xn
          dxdz(j,j) = -Vez / Xn
          do k = 1, 3
             dzdu(j,k) = dzdu(j,k) - U(j)*U(k) / Un3
             dxdv(j,k) = dxdv(j,k) - ez(j)*ez(k) / Xn - ex(j)*ex(k) / Xn
             dxdz(j,k) = dxdz(j,k) + ex(j)*Vez*V(k) / Xn2 - ez(j)*V(k) / Xn
          end do
       end do

       ! Levi-Civita symbols
       dydz(1,2) =  ex(3)
       dydz(1,3) = -ex(2)
       dydz(2,1) = -ex(3)

       dydz(2,3) =  ex(1)
       dydz(3,1) =  ex(2)
       dydz(3,2) = -ex(1)

       dydx(1,2) = -ez(3)
       dydx(1,3) =  ez(2)
       dydx(2,1) =  ez(3)

       dydx(2,3) = -ez(1)
       dydx(3,1) = -ez(2)
       dydx(3,2) =  ez(1)

       ! Derivatives - vector/site
       do j = 1, 3
          dudri(j,j)  =  Vn + Cn
          dudriz(j,j) = -Vn
          dudrix(j,j) = -Cn
          dvdri(j,j)  =  1.d0
          dvdrix(j,j) = -1.d0
          do k = 1, 3
             dudri(j,k)  = dudri(j,k)  + V(j)*C(k) / Cn + C(j)*V(k) / Vn
             dudriz(j,k) = dudriz(j,k) - V(j)*C(k) / Cn
             dudrix(j,k) = dudrix(j,k) - C(j)*V(k) / Vn
          end do
       end do

       ! Combine (vec/vec).(vec/site)

       ! ez derivatives
       do l = 1, 3 
          do j = 1, 3
             do k = 1,3
                ezi(l,j)  = ezi(l,j)  + dzdu(l,k)*dudri(k,j)
                eziz(l,j) = eziz(l,j) + dzdu(l,k)*dudriz(k,j)
                ezix(l,j) = ezix(l,j) + dzdu(l,k)*dudrix(k,j)
             end do
          end do
       end do

       ! ex derivatives
       do l = 1, 3
          do j = 1, 3
             do k = 1, 3
                exi(l,j)  = exi(l,j)  + dxdv(l,k)*dvdri(k,j)  + dxdz(l,k)*ezi(k,j)
                exiz(l,j) = exiz(l,j) + dxdz(l,k)*eziz(k,j)
                exix(l,j) = exix(l,j) + dxdv(l,k)*dvdrix(k,j) + dxdz(l,k)*ezix(k,j)
             end do
          end do
       end do

       ! ey derivatives
       do l = 1, 3
          do j = 1, 3
             do k = 1, 3
                eyi(l,j)  = eyi(l,j)  + dydx(l,k)*exi(k,j)  + dydz(l,k)*ezi(k,j)
                eyiz(l,j) = eyiz(l,j) + dydx(l,k)*exiz(k,j) + dydz(l,k)*eziz(k,j)
                eyix(l,j) = eyix(l,j) + dydx(l,k)*exix(k,j) + dydz(l,k)*ezix(k,j)
             end do
          end do
       end do

       ! rotation matrix derivatives
       do j = 1, 3
          do k = 1, 3
             dx(k,j,1,i)  = exi(j,k)
             dx(k,j,2,i)  = eyi(j,k)
             dx(k,j,3,i)  = ezi(j,k)
             dxz(k,j,1,i) = exiz(j,k)
             dxz(k,j,2,i) = eyiz(j,k)
             dxz(k,j,3,i) = eziz(j,k)
             dxx(k,j,1,i) = exix(j,k)
             dxx(k,j,2,i) = eyix(j,k)
             dxx(k,j,3,i) = ezix(j,k)
          end do
       end do

    end do
  end subroutine

  !---------------------------------------------------------------------+
  ! Calculate atomic forces using derivatives of rotation matrices      !
  !---------------------------------------------------------------------+
  subroutine atomicForces(d0, q0, o0, h0, dd0, dq0, qq0, d1v, d2v, d3v, &
                  d4v, x, dx, dxx, dxz, fa, fCM, convFactor, nM)

    real(dp) d0(3), q0(3,3), o0(3,3,3), h0(3,3,3,3), &
             dd0(3,3), dq0(3,3,3), qq0(3,3,3,3), &
             d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3), &
             d4v(3,3,3,3,maxCoo/3), x(3,3,maxCoo/3), dx(3,3,3,maxCoo/3), &
             dxx(3,3,3,maxCoo/3), dxz(3,3,3,maxCoo/3), fa(maxCoo), &
             fCM(3,maxCoo/3), convFactor

    integer nM, iO, iH1, iH2, i, ii, j, jj, k, kk, l, ll, m, y
    real(dp) Fi(3), Fix(3), Fiz(3), mO, mH

    mO = 16.d0 / 18.d0
    mH = 1.d0 / 18.d0

    do m = 1, nM
       ! Zeroise
       Fi(:)  = 0.d0
       Fix(:) = 0.d0
       Fiz(:) = 0.d0

       iO  = 3 * (m-1+2*nM)
       iH1 = 6 * (m-1)
       iH2 = 3 + 6*(m-1)

       do y = 1, 3 ! force component (xyz)
          do i = 1, 3
             do ii = 1, 3
                ! static-dipole
                Fi(y) = Fi(y) + d0(ii) * dx(y,i,ii,m) * d1v(i,m)
                Fix(y) = Fix(y) + d0(ii) * dxx(y,i,ii,m) * d1v(i,m)
                Fiz(y) = Fiz(y) + d0(ii) * dxz(y,i,ii,m) * d1v(i,m)

                do j = 1, 3
                   do jj = 1, 3
                      ! static-quadrupole
                      Fi(y) = Fi(y) + 1.d0/3.d0 * (dx(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dx(y,j,jj,m)) * q0(ii,jj) * d2v(i,j,m)
                      Fix(y) = Fix(y) + 1.d0/3.d0 * (dxx(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dxx(y,j,jj,m)) * q0(ii,jj) * d2v(i,j,m)
                      Fiz(y) = Fiz(y) + 1.d0/3.d0 * (dxz(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dxz(y,j,jj,m)) * q0(ii,jj) * d2v(i,j,m)

                      ! dipole-dipole polarization
                      Fi(y) = Fi(y) - 1.d0/2.d0 * (dx(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dx(y,j,jj,m)) * dd0(ii,jj) &
                              * d1v(i,m) * d1v(j,m)
                      Fix(y) = Fix(y) - 1.d0/2.d0 * (dxx(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dxx(y,j,jj,m)) * dd0(ii,jj) &
                              * d1v(i,m) * d1v(j,m)
                      Fiz(y) = Fiz(y) - 1.d0/2.d0 * (dxz(y,i,ii,m) * x(j,jj,m) &
                              + x(i,ii,m) * dxz(y,j,jj,m)) * dd0(ii,jj) &
                              * d1v(i,m) * d1v(j,m)

                      do k = 1, 3
                         do kk = 1, 3
                            ! static-octupole
                            Fi(y) = Fi(y) + 1.d0 / 15.d0 * o0(ii,jj,kk) * d3v(i,j,k,m) &
                                    * (dx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dx(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dx(y,k,kk,m))
                            Fix(y) = Fix(y) + 1.d0 / 15.d0 * o0(ii,jj,kk) * d3v(i,j,k,m) &
                                    * (dxx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dxx(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dxx(y,k,kk,m))
                            Fiz(y) = Fiz(y) + 1.d0 / 15.d0 * o0(ii,jj,kk) * d3v(i,j,k,m) &
                                    * (dxz(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dxz(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dxz(y,k,kk,m))

                            ! dipole-quadrupole polarizability
                            Fi(y) = Fi(y) - 1.d0 / 3.d0 &
                                    * dq0(ii,jj,kk) * d1v(i,m) * d2v(j,k,m) &
                                    * (dx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dx(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dx(y,k,kk,m))
                            Fix(y) = Fix(y) - 1.d0 / 3.d0 &
                                    * dq0(ii,jj,kk) * d1v(i,m) * d2v(j,k,m) &
                                    * (dxx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dxx(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dxx(y,k,kk,m))
                            Fiz(y) = Fiz(y) - 1.d0 / 3.d0 &
                                    * dq0(ii,jj,kk) * d1v(i,m) * d2v(j,k,m) &
                                    * (dxz(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * dxz(y,j,jj,m) * x(k,kk,m) &
                                    +  x(i,ii,m) * x(j,jj,m) * dxz(y,k,kk,m))

                            do l = 1, 3
                               do ll = 1, 3
                                  ! static-hexadecapole
                                  Fi(y) = Fi(y) + 1.d0 / 105.d0 &
                                        * h0(ii,jj,kk,ll) * d4v(i,j,k,l,m) &
                                        * (dx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dx(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dx(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dx(y,l,ll,m))
                                  Fix(y) = Fix(y) + 1.d0 / 105.d0 &
                                        * h0(ii,jj,kk,ll) * d4v(i,j,k,l,m) &
                                        * (dxx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dxx(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dxx(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dxx(y,l,ll,m))
                                  Fiz(y) = Fiz(y) + 1.d0 / 105.d0 &
                                        * h0(ii,jj,kk,ll) * d4v(i,j,k,l,m) &
                                        * (dxz(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dxz(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dxz(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dxz(y,l,ll,m))

                                  ! quadrupole-quadrupole polarizability
                                  Fi(y) = Fi(y) - 1.d0 / 6.d0 &
                                        * qq0(ii,jj,kk,ll) * d2v(i,j,m) * d2v(k,l,m) &
                                        * (dx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dx(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dx(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dx(y,l,ll,m))
                                  Fix(y) = Fix(y) - 1.d0 / 6.d0 &
                                        * qq0(ii,jj,kk,ll) * d2v(i,j,m) * d2v(k,l,m) &
                                        * (dxx(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dxx(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dxx(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dxx(y,l,ll,m))
                                  Fiz(y) = Fiz(y) - 1.d0 / 6.d0 &
                                        * qq0(ii,jj,kk,ll) * d2v(i,j,m) * d2v(k,l,m) &
                                        * (dxz(y,i,ii,m) * x(j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * dxz(y,j,jj,m) * x(k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * dxz(y,k,kk,m) * x(l,ll,m) &
                                        +  x(i,ii,m) * x(j,jj,m) * x(k,kk,m) * dxz(y,l,ll,m))
                                end do
                            end do
                         end do
                      end do
                   end do
                end do
             end do
          end do

          fa(iO+y)  = (fCM(y,m) * mO - Fi(y) * mO) * convFactor
          fa(iH1+y) = (fCM(y,m) * mH - Fi(y) * mH - Fix(y)) * convFactor
          fa(iH2+y) = (fCM(y,m) * mH - Fi(y) * mH - Fiz(y)) * convFactor

        end do
    end do
  end subroutine

  !-------------------------------------------------------------------+
  ! Calculate forces due to flexible moments - dipole and quadrupole  !
  !-------------------------------------------------------------------+
  subroutine atomicFFlexible(qatoms, d1v, d2v, ra, rCM, fa, convFactor, &
                  kk1, kk2, nM, dqdms, qO0, qH0, fact_a, fact_b, fact_c, &
                  d1, d2, dd1, dd2)
    
    real(dp) qatoms(maxCoo/3), d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), &
             ra(maxCoo), rCM(3,maxCoo/3), fa(maxCoo), &
             convFactor, fO, fH1, fH2, kk1, kk2, &
             dqdms(3,3,3,maxCoo/3), d1(3,maxCoo/9), d2(3,maxCoo/9), &
             dd1(3,3,3,maxCoo/9), dd2(3,3,3,maxCoo/9)

    real(dp) d1Va(3,3), rMol(3), &
             dDrO, dDrH1, dDrH2, qDrO, qDrH1, qDrH2, &
             rO, rH1, rH2, dr_O(3), dr_H1(3), dr_h2(3), &
             mO, mH, qO0, qH0, fact_a, fact_b, qFrO, qFrH1, qFrH2, &
             fact_c, LDrO, LDrH1, LDrH2, &
             LFrO, LFrH1, LFrH2, rL1, rL2, dr_L1(3), dr_L2(3)

    integer nM, iO, iH1, iH2, m, y, i, j, k, n

    mO = 16.d0/18.d0
    mH = 1.d0/18.d0

    do m = 1, nM
       ! 
       iO  = 3 * (m-1+2*nM)
       iH1 = 6 * (m-1)
       iH2 = 3 + 6*(m-1)

       do i = 1, 3
          dr_O(i)  =  ra(iO+i) - rCM(i,m)
          dr_H1(i) = ra(iH1+i) - rCM(i,m)
          dr_H2(i) = ra(iH2+i) - rCM(i,m)
          dr_L1(i) = d1(i,m) !- rCM(i,m)
          dr_L2(i) = d2(i,m) !- rCM(i,m)
       end do

       rO  =  dr_O(1)**2 +  dr_O(2)**2 +  dr_O(3)**2
       rH1 = dr_H1(1)**2 + dr_H1(2)**2 + dr_H1(3)**2
       rH2 = dr_H2(1)**2 + dr_H2(2)**2 + dr_H2(3)**2
       rL1 = dr_L1(1)**2 + dr_L1(2)**2 + dr_L1(3)**2
       rL2 = dr_L2(1)**2 + dr_L2(2)**2 + dr_L2(3)**2

       ! Two parts: derivative of charge (H1,H2,O) due to displacement of
       ! H1, H2 or O (Dqr). Derivative of the position of charge H1, H2 or O (qDr)
       do y = 1, 3
          ! Zeroise
          dDrO  = 0.d0
          dDrH1 = 0.d0
          dDrH2 = 0.d0
          qDrO  = 0.d0
          qDrH1 = 0.d0
          qDrH2 = 0.d0
          qFrO  = 0.d0
          qFrH1 = 0.d0
          qFrH2 = 0.d0
          LDrO  = 0.d0
          LDrH1 = 0.d0
          LDrH2 = 0.d0
          LFrO  = 0.d0
          LFrH1 = 0.d0
          LFrH2 = 0.d0

          ! Force due too qDr from dipole contribution
          dDrO  = qatoms((m-1)*3+1) * d1v(y,m)
          dDrH1 = qatoms((m-1)*3+2) * d1v(y,m)
          dDrH2 = qatoms((m-1)*3+3) * d1v(y,m)

          ! Force due to Dqr - from c++ to fortran, matrix is transposed...
          ! Force due to qDr from quadrupole contribution
          do i = 1, 3
             ! Dipole component
             dDrO  = dDrO + (dqdms(y,3,3,m)*ra(iO+i) &
                          +  dqdms(y,2,3,m)*ra(iH2+i) &
                          +  dqdms(y,1,3,m)*ra(iH1+i)) * d1v(i,m)
             dDrH1 = dDrH1 + (dqdms(y,3,1,m)*ra(iO+i) &
                           +  dqdms(y,2,1,m)*ra(iH2+i) &
                           +  dqdms(y,1,1,m)*ra(iH1+i)) * d1v(i,m)
             dDrH2 = dDrH2 + (dqdms(y,3,2,m)*ra(iO+i) &
                           +  dqdms(y,2,2,m)*ra(iH2+i) &
                           +  dqdms(y,1,2,m)*ra(iH1+i)) * d1v(i,m)

             do j = 1, 3
                ! Dqr component
                qDrO  = qDrO + (dqdms(y,2,3,m)*dr_H2(i)*dr_H2(j) &
                             +  dqdms(y,1,3,m)*dr_H1(i)*dr_H1(j))&
                             *  3.d0/2.d0*d2v(i,j,m)
                qDrH1 = qDrH1 + (dqdms(y,2,1,m)*dr_H2(i)*dr_H2(j) &
                              +  dqdms(y,1,1,m)*dr_H1(i)*dr_H1(j))&
                              *  3.d0/2.d0*d2v(i,j,m)
                qDrH2 = qDrH2 + (dqdms(y,2,2,m)*dr_H2(i)*dr_H2(j) &
                              +  dqdms(y,1,2,m)*dr_H1(i)*dr_H1(j))&
                              *  3.d0/2.d0*d2v(i,j,m)

                LDrO  = LDrO + (dqdms(y,2,3,m)*dr_L2(i)*dr_L2(j) &
                             +  dqdms(y,1,3,m)*dr_L1(i)*dr_L1(j))&
                             *  3.d0/2.d0*d2v(i,j,m)
                LDrH1 = LDrH1 + (dqdms(y,2,1,m)*dr_L2(i)*dr_L2(j) &
                              +  dqdms(y,1,1,m)*dr_L1(i)*dr_L1(j))&
                              *  3.d0/2.d0*d2v(i,j,m)
                LDrH2 = LDrH2 + (dqdms(y,2,2,m)*dr_L2(i)*dr_L2(j) &
                              +  dqdms(y,1,2,m)*dr_L1(i)*dr_L1(j))&
                              *  3.d0/2.d0*d2v(i,j,m)

                LDrO = LDrO + d2v(i,j,m) * 3.d0 &
                            * (qatoms((m-1)*3+2)*dd1(y,i,1,m)*dr_L1(j) &
                            +  qatoms((m-1)*3+2)*dd2(y,i,1,m)*dr_L2(j))
                LDrH1 = LDrH1 + d2v(i,j,m) * 3.d0 &
                              * (qatoms((m-1)*3+2)*dd1(y,i,2,m)*dr_L1(j) &
                              +  qatoms((m-1)*3+2)*dd2(y,i,2,m)*dr_L2(j))
                LDrH2 = LDrH2 + d2v(i,j,m) * 3.d0  &
                              * (qatoms((m-1)*3+2)*dd1(y,i,3,m)*dr_L1(j) &
                              +  qatoms((m-1)*3+2)*dd2(y,i,3,m)*dr_L2(j))

                LFrO = LFrO + d2v(i,j,m) * 3.d0 &
                            * (qH0*(dd1(y,i,1,m))*dr_L1(j) &
                            +  qH0*(dd2(y,i,1,m))*dr_L2(j))
                LFrH1 = LFrH1 + d2v(i,j,m) * 3.d0 &
                              * (qH0*(dd1(y,i,2,m))*dr_L1(j) &
                              +  qH0*(dd2(y,i,2,m))*dr_L2(j))
                LFrH2 = LFrH2 + d2v(i,j,m) * 3.d0 &
                              * (qH0*(dd1(y,i,3,m))*dr_L1(j) &
                              +  qH0*(dd2(y,i,3,m))*dr_L2(j))

                if (i.eq.y) then ! and if (j.eq.y) / same through symmetry of d2v
                    qDrO = qDrO + d2v(y,j,m) * 3.d0 &
                                * (qatoms((m-1)*3+2)*(-mO)*dr_H1(j) &
                                +  qatoms((m-1)*3+3)*(-mO)*dr_H2(j))
                    qDrH1 = qDrH1 + d2v(y,j,m) * 3.d0 &
                                  * (qatoms((m-1)*3+2)*(1.d0-mH)*dr_H1(j) &
                                  +  qatoms((m-1)*3+3)*(-mH)*dr_H2(j))
                    qDrH2 = qDrH2 + d2v(y,j,m) * 3.d0 &
                                  * (qatoms((m-1)*3+2)*(-mH)*dr_H1(j) &
                                  +  qatoms((m-1)*3+3)*(1.d0-mH)*dr_H2(j))

                    qFrO = qFrO + d2v(y,j,m) * 3.d0 &
                               * (qH0*(-mO)*dr_H1(j) &
                               +  qH0*(-mO)*dr_H2(j))
                    qFrH1 = qFrH1 + d2v(y,j,m) * 3.d0 &
                                 * (qH0*(1.d0-mH)*dr_H1(j) &
                                 +  qH0*(-mH)*dr_H2(j))
                    qFrH2 = qFrH2 + d2v(y,j,m) * 3.d0 &
                                  * (qH0*(-mH)*dr_H1(j) &
                                  +  qH0*(1.d0-mH)*dr_H2(j))
                end if

                if (j.eq.i) then ! Move to top level (i.e. ii)
                   qDrO  = qDrO - (dqdms(y,2,3,m)*rH2 &
                                +  dqdms(y,1,3,m)*rH1)&
                                *  0.5d0*d2v(i,j,m)
                   qDrH1 = qDrH1 - (dqdms(y,2,1,m)*rH2 &
                                 +  dqdms(y,1,1,m)*rH1)&
                                 *  0.5d0*d2v(i,j,m)
                   qDrH2 = qDrH2 - (dqdms(y,2,2,m)*rH2 &
                                 +  dqdms(y,1,2,m)*rH1)&
                                 *  0.5d0*d2v(i,j,m)

                   LDrO  = LDrO - (dqdms(y,2,3,m)*rL2 &
                                +  dqdms(y,1,3,m)*rL1)&
                                *  0.5d0*d2v(i,j,m)
                   LDrH1 = LDrH1 - (dqdms(y,2,1,m)*rL2 &
                                 +  dqdms(y,1,1,m)*rL1)&
                                 *  0.5d0*d2v(i,j,m)
                   LDrH2 = LDrH2 - (dqdms(y,2,2,m)*rL2 &
                                 +  dqdms(y,1,2,m)*rL1)&
                                 *  0.5d0*d2v(i,j,m)

                   qFrO  = qFrO - d2v(i,j,m) &
                                * (qH0*(-mO)*dr_H1(y) &
                                +  qH0*(-mO)*dr_H2(y))
                   qFrH1 = qFrH1 - d2v(i,j,m) &
                                 * (qH0*(1.d0-mH)*dr_H1(y) &
                                 +  qH0*(-mH)*dr_H2(y))
                   qFrH2 = qFrH2 - d2v(i,j,m) &
                                 * (qH0*(-mH)*dr_H1(y) &
                                 +  qH0*(1.d0-mH)*dr_H2(y))

                   qDrO  = qDrO - d2v(i,j,m) &
                                * (qatoms((m-1)*3+2)*(-mO)*dr_H1(y) &
                                +  qatoms((m-1)*3+3)*(-mO)*dr_H2(y))
                   qDrH1 = qDrH1 - d2v(i,j,m) &
                                 * (qatoms((m-1)*3+2)*(1.d0-mH)*dr_H1(y) &
                                 +  qatoms((m-1)*3+3)*(-mH)*dr_H2(y))
                   qDrH2 = qDrH2 - d2v(i,j,m) &
                                 * (qatoms((m-1)*3+2)*(-mH)*dr_H1(y) &
                                 +  qatoms((m-1)*3+3)*(1.d0-mH)*dr_H2(y))

                   !Cross product components - Move to ii level!
                   do k = 1, 3
                      LDrO  = LDrO - d2v(i,j,m) &
                                   * (qatoms((m-1)*3+2)*dd1(y,k,1,m)*dr_L1(k) &
                                   +  qatoms((m-1)*3+3)*dd2(y,k,1,m)*dr_L2(k))
                      LDrH1 = LDrH1 - d2v(i,j,m) &
                                    * (qatoms((m-1)*3+2)*dd1(y,k,2,m)*dr_L1(k) &
                                    +  qatoms((m-1)*3+3)*dd2(y,k,2,m)*dr_L2(k))
                      LDrH2 = LDrH2 - d2v(i,j,m) &
                                    * (qatoms((m-1)*3+2)*dd1(y,k,3,m)*dr_L1(k) &
                                    +  qatoms((m-1)*3+3)*dd2(y,k,3,m)*dr_L2(k))

                      LFrO  = LFrO - d2v(i,j,m) &
                                   * (qH0*dd1(y,k,1,m)*dr_L1(k) &
                                   +  qH0*dd2(y,k,1,m)*dr_L2(k))
                      LFrH1 = LFrH1 - d2v(i,j,m) &
                                    * (qH0*dd1(y,k,2,m)*dr_L1(k) &
                                    +  qH0*dd2(y,k,2,m)*dr_L2(k))
                      LFrH2 = LFrH2 - d2v(i,j,m) &
                                    * (qH0*dd1(y,k,3,m)*dr_L1(k) &
                                    +  qH0*dd2(y,k,3,m)*dr_L2(k))
                   end do

                 end if
             end do
          end do

          fO  = dDrO  &
              + (qDrO * fact_a + qFrO * fact_b &
              +  LDrO * fact_a * fact_c + LFrO * fact_b * fact_c) / 3.d0
          fH1 = dDrH1 & 
              + (qDrH1 * fact_a + qFrH1 * fact_b &
              +  LDrH1 * fact_a * fact_c + LFrH1 * fact_b * fact_c) / 3.d0
          fH2 = dDrH2 &
              + (qDrH2 * fact_a + qFrH2 * fact_b &
              +  LDrH2 * fact_a * fact_c + LFrH2 * fact_b * fact_c) / 3.d0

          fa(iO+y)  = fa(iO+y)  - fO  * convFactor * kk1 * kk2
          fa(iH1+y) = fa(iH1+y) - fH1 * convFactor * kk1 * kk2
          fa(iH2+y) = fa(iH2+y) - fH2 * convFactor * kk1 * kk2
       end do
    end do

  end subroutine

  !--------------------------------------------------------------------+ 
  ! Find position of dummy atoms and calculate derivatives             ! 
  !--------------------------------------------------------------------+
  subroutine dummyAtoms(mol, rCM, x, d1, d2, dd1, dd2, f_req, t_req, &
                  & f_ang, t_ang, dx, dxx, dxz)

    real(dp) mol(9), rCM(3), x(3,3), d1(3), d2(3), dd1(3,3,3), dd2(3,3,3), V(3), C(3), &
             XA(3), XB(3), XAn, XBn, ez(3), Vez, Cez, ang, pi, cs, &
             ss, r_eq, a_eq, P1(3), P2(3), PA(3), PB(3), pav, pbv, r_h1, r_h2, &
             dr_h1(3), dr_h2(3), dr1, dr2, f_req, f_ang, Vn, Cn, c_cos, dang, c_ang, &
             t_req, t_ang, dcs_o(3), dcs_h1(3), dcs_h2(3), dr1dr2, &
             df_cs, df_ss, dx(3,3,3), dxx(3,3,3), dxz(3,3,3), mO, mH, df_r1, df_r2, &
             Vx(3,3), Cx(3,3)
    integer i, j, k

    mO = 16.d0 / 18.d0
    mH = 1.d0  / 18.d0

    pi   = 3.14159265358979324d0
    r_eq = 0.957835834262811d0
    a_eq = 104.50781762499885d0 * pi / 180.d0

    ang = 106.45d0 / 2.d0 * pi / 180.d0 * t_ang

    dr1dr2 = 0.d0

    do i = 1, 3
       dr_h1(i) = mol(i) - mol(i+3)
       dr_h2(i) = mol(i) - mol(i+6)
       dr1dr2 = dr1dr2 + dr_h1(i)*dr_h2(i)
    end do

    r_h1 = sqrt(dr_h1(1)**2 + dr_h1(2)**2 + dr_h1(3)**2)
    r_h2 = sqrt(dr_h2(1)**2 + dr_h2(2)**2 + dr_h2(3)**2)

    dr1 = r_h1 - r_eq
    dr2 = r_h2 - r_eq

    df_r1 = r_eq * t_req + dr1**2 * f_req
    df_r2 = r_eq * t_req + dr2**2 * f_req

    do i = 1, 3
       V(i)  = rCM(i) - mol(i+3)
       C(i)  = rCM(i) - mol(i+6)
       ez(i) = x(i,3)
    end do

    ! Cross product matrices
    Vx(1,2) = -V(3)
    Vx(1,3) =  V(2)
    Vx(2,1) =  V(3)
    Vx(2,3) = -V(1)
    Vx(3,1) = -V(2)
    Vx(3,2) =  V(1)

    Cx(1,2) = -C(3)
    Cx(1,3) =  C(2)
    Cx(2,1) =  C(3)
    Cx(2,3) = -C(1)
    Cx(3,1) = -C(2)
    Cx(3,2) =  C(1)

    ! Calculate angle
    c_cos = dr1dr2 / (r_h1 * r_h2)
    c_ang = acos(c_cos)

    dang = c_ang - a_eq

    cs = cos(ang + f_ang * dang)
    ss = sin(ang + f_ang * dang)

    Vez = V(1)*ez(1) + V(2)*ez(2) + V(3)*ez(3)
    Cez = C(1)*ez(1) + C(2)*ez(2) + C(3)*ez(3)

    do i = 1, 3
       XA(i) = V(i) - Vez * ez(i)
       XB(i) = C(i) - Cez * ez(i)
    end do

    XAn = sqrt(XA(1)**2 + XA(2)**2 + XA(3)**2)
    XBn = sqrt(XB(1)**2 + XB(2)**2 + XB(3)**2)

    do i = 1, 3
       XA(i) = XA(i) / XAn
       XB(i) = XB(i) / XBn
    end do

    PA(1) = (ez(2)*XA(3) - XA(3)*V(2))
    PA(2) = (ez(3)*XA(1) - XA(1)*V(3))
    PA(3) = (ez(1)*XA(2) - XA(2)*V(1))

    PB(1) = (ez(2)*C(3) - ez(3)*C(2))
    PB(2) = (ez(3)*C(1) - ez(1)*C(3))
    PB(3) = (ez(1)*C(2) - ez(2)*C(1))

    do i = 1, 3
       d1(i) = ez(i) * cs !* ez(i) !+ rCM(i) !- PA(i)
       d2(i) = ez(i) * cs !* ez(i) !+ rCM(i) !- PB(i)
    end do

    ! Calculate derivatives of rotation/translation operator
    ! Zeroise
    dd1(:,:,:) = 0.d0
    dd2(:,:,:) = 0.d0

    ! d/dr cos(arccos(O(r)) : (-sin(O(r))(-)1/sqrt(1-O(r)**2)*O'(r)
    df_cs =  ss / (sqrt(1.d0 - c_cos**2)) * f_ang
    df_ss = -cs / (sqrt(1.d0 - c_cos**2)) * f_ang

    ! Derivative of angle function -- RE-USE!
    do i = 1, 3
       dcs_o(i)  = ((dr_h1(i) + dr_h2(i)) / (r_h1 * r_h2) &
                 - c_cos * dr_h1(i) / r_h1**2 - c_cos * dr_h2(i) / r_h2**2)
       dcs_h1(i) = (-dr_h2(i) / (r_h1 * r_h2) + c_cos * dr_h1(i) / r_h1**2)
       dcs_h2(i) = (-dr_h1(i) / (r_h1 * r_h2) + c_cos * dr_h2(i) / r_h2**2)
    end do

    ! Combine into 3x3 
    ! d/dr R(r)ez(r)f(r) : R'(r)ez(r)f(r) + R(r)ez'(r)f(r) + R(r)ez(r)f'(r)
    do i = 1, 3
       do j = 1, 3
         dd1(i,j,2) = dd1(i,j,2) + cs * (dx(i,j,3) * mH + dxx(i,j,3)) &
                    + dcs_h1(i) * (ez(j) * df_cs - PA(j) * df_ss)
         dd1(i,j,3) = dd1(i,j,3) + cs * (dx(i,j,3) * mH + dxz(i,j,3)) &
                    + dcs_h2(i) * (ez(j) * df_cs - PA(j) * df_ss)
         dd1(i,j,1) = dd1(i,j,1) + cs * dx(i,j,3) * mO &
                    + dcs_o(i) * (ez(j) * df_cs - PA(j) * df_ss)

         dd2(i,j,2) = dd2(i,j,2) + cs * (dx(i,j,3) * mH + dxx(i,j,3)) &
                    + dcs_h1(i) * (ez(j) * df_cs + PB(j) / XBn * df_ss)
         dd2(i,j,3) = dd2(i,j,3) + cs * (dx(i,j,3) * mH + dxz(i,j,3)) &
                    + dcs_h2(i) * (ez(j) * df_cs + PB(j) / XBn * df_ss)
         dd2(i,j,1) = dd2(i,j,1) + cs * dx(i,j,3) * mO &
                    + dcs_o(i) * (ez(j) * df_cs + PB(j) / XBn * df_ss)

         ! Cross product of principal vector and ez rotation derivative
         do k = 1, 3
            dd1(i,j,2) = dd1(i,j,2) + ss / XAn * Vx(j,k) * (dx(i,k,3) * mH + dxx(i,k,3))
            dd1(i,j,3) = dd1(i,j,3) + ss / XAn * Vx(j,k) * (dx(i,k,3) * mH + dxz(i,k,3))
            dd1(i,j,1) = dd1(i,j,1) + ss / XAn * Vx(j,k) * dx(i,k,3) * mO

            dd2(i,j,2) = dd2(i,j,2) + ss / XBn * Cx(j,k) * (dx(i,k,3) * mH + dxx(i,k,3))
            dd2(i,j,3) = dd2(i,j,3) + ss / XBn * Cx(j,k) * (dx(i,k,3) * mH + dxz(i,k,3))
            dd2(i,j,1) = dd2(i,j,1) + ss / XBn * Cx(j,k) * dx(i,k,3) * mO
         end do
       end do
    end do

    !print *, dd1(:,:,1)
    !print *, dd1(:,:,2)
    !print *, dd1(:,:,3)

  end subroutine

end module molecProperties
