from ase.io import read
from ase.io.trajectory import Trajectory

traj = Trajectory('data/ice-scme-0.928000-new.traj')
ice = traj[-1]

angles = []
oh_bonds = []
oo_bonds = []

nmols = len(ice) // 3

for i in range(len(ice)//3):
    angles.append(ice.get_angle(i*3+1,3*i,i*3+2))

import numpy as np
angles = np.array(angles)

o_idx = np.arange(0,len(ice),3)

print(np.mean(angles),np.max(angles),np.min(angles))

for a in o_idx:
    for b in o_idx:
        if a == b:
            continue
        oo_bonds.append(ice.get_distance(a,b))

oo_bonds = np.array(oo_bonds)

x = oo_bonds <= 3.1

print(np.mean(oo_bonds[x]),np.max(oo_bonds[x]),np.min(oo_bonds[x]))

for i in range(len(ice)//3):
    oh_bonds.append(ice.get_distance(3*i,i*3+1))
    oh_bonds.append(ice.get_distance(3*i,i*3+2))

oh_bonds = np.array(oh_bonds)

print(np.mean(oh_bonds),np.max(oh_bonds),np.min(oh_bonds))
#print(oh_bonds)
