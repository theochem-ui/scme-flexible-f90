from ase import Atoms
import numpy as np
from ase_interface_mpi import SCME_PS

data = open('ts-ccpol.xyz', 'r')
lines = data.readlines()
nDimers = len(lines) // 8

te=1.10738464e+00  
td=9.00000000e+00  
Ar=5.00000000e+03 
br=-2.50000000e-01
cr=-3.43772922e+00

#te=1.20363264e+00  
#td=5.36055971e+00  
#Ar=9.63937695e+03 
#br=-7.18544048e-01
#cr=-3.55369543e+00

def get_dimer_energies(p):

    energies_o = np.zeros(nDimers)
    energies_n = np.zeros(nDimers)
    # Starting from line 2 (1) energies are 
    # at every 8th
    points = np.arange(1,len(lines),8)
    
    for i, val in enumerate(points):
        energies_o[i] = float(lines[val].split()[0])
    
    from ase.units import kcal, mol
    energies_o *= kcal / mol

    for i in range(nDimers):
        water = Atoms('OHHOHH')
        pos = np.zeros((6,3))
    
        # Water 1
        pos[0,0] = float(lines[i*8+2].split()[1])
        pos[0,1] = float(lines[i*8+2].split()[2])
        pos[0,2] = float(lines[i*8+2].split()[3])
        pos[1,0] = float(lines[i*8+3].split()[1])
        pos[1,1] = float(lines[i*8+3].split()[2])
        pos[1,2] = float(lines[i*8+3].split()[3])
        pos[2,0] = float(lines[i*8+4].split()[1])
        pos[2,1] = float(lines[i*8+4].split()[2])
        pos[2,2] = float(lines[i*8+4].split()[3])
    
        # Water 2
        pos[3,0] = float(lines[i*8+5].split()[1])
        pos[3,1] = float(lines[i*8+5].split()[2])
        pos[3,2] = float(lines[i*8+5].split()[3])
        pos[4,0] = float(lines[i*8+6].split()[1])
        pos[4,1] = float(lines[i*8+6].split()[2])
        pos[4,2] = float(lines[i*8+6].split()[3])
        pos[5,0] = float(lines[i*8+7].split()[1])
        pos[5,1] = float(lines[i*8+7].split()[2])
        pos[5,2] = float(lines[i*8+7].split()[3])
    
        water.set_positions(pos)
        water.center(vacuum=12.5)

        water.calc = SCME_PS(water,
                     numerical=False,
                     static_flag='dqoh', # <--- FLAG to turn on/off static moments
                     ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                     flex_flag='dq',
                     te=p[0],
                     td=p[1],
                     Ar=p[2],
                     br=p[3],
                     cr=p[4])

        energies_n[i] = water.get_potential_energy()

    print(0.5 * np.sqrt(((energies_n - energies_o)**2).sum()))
    return energies_n - energies_o

from scipy.optimize import least_squares

bounds = np.array([[1.0, 2.0], 
                   [4.0, 11.0], 
                   [0.5e+03, 4.0e+04],
                   [-4.0, -0.05],
                   [-4.0, -0.5]])
 
res = least_squares(get_dimer_energies, 
                    np.array([te, td, Ar, br, cr]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=None, xtol=1e-10, 
                    max_nfev=100, verbose=1)
print(res.x)

