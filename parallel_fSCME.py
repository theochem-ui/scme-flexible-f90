from ase import Atoms
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase_interface_mpi import SCME_PS
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
import numpy as np
import ase.parallel as mpi

te=1.10036499e+00
td=7.09887127e+00
AO=8.66033865e+03
bO=-5.64462066e-01
cO=-3.48562837e+00

# Read in previous from tip3p
#traj = Trajectory('fSCME_729mol_equil-U.traj')
#atoms = traj[-1]

# Get rank
rank = mpi.world.rank
print(mpi.world.size)
print(mpi.world.rank)

for i in [mpi.world.rank]:
    traj = Trajectory('ctd-fSCME-%d-E.traj' %i)
    atoms = traj[-1]
    # Equilibrate - 729 molecules
    tag = 'ctd-fSCME-%d-F' %i

    dT = float(i) * 10.

    atoms.set_constraint()  # remove constraints
    atoms.calc = SCME_PS(atoms,
                         numerical=False,
                         te=te,
                         td=td,
                         AO=AO,
                         bO=bO,
                         cO=cO)
    
    md = Langevin(atoms, 0.5 * units.fs, temperature=(300 + dT) * units.kB ,
                  friction=0.01, logfile=tag + '.log')
    traj = Trajectory(tag + '.traj', 'w', atoms, master=mpi.world.rank)
    md.attach(traj.write, interval=5)
    md.run(10000)
