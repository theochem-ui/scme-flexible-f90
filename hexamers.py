import numpy as np
import pylab as pl

names = ['prism', 'cage', 'book-1', 'book-2',
         'cyclic-ring','bag','cyclic-boat-1','cyclic-boat-2']

# prism, cage, book-1, book-2, bag, cyclic-boat-1, cyclic-boat-2
total_mp2 = np.array([-458.296817, -458.296753, -458.296426, 
                      -458.295839, -458.295335, -458.294962,
                      -458.293693, -458.293581])

from ase.units import kcal, mol, Hartree
from ase.io import read
from ase_interface_mpi import SCME_PS
from ase.optimize import BFGS
from ase.io.trajectory import Trajectory as Tr

# NEW set
te=1.13922136e+00  
td=8.50782754e+00  
Ar=7.70884702e+03 
br=-5.36242472e-01
cr=-3.48164066e+00

#te=1.22120290e+00  
#td=4.99172506e+00  
#Ar=7.36603824e+03 
#br=-3.25662200e-01
#cr=-3.62777285e+00

# Optimal ATM
#te=1.10454150e+00                                                                               
#td=7.55480010e+00  
#Ar=8.14963246e+03 
#br=-5.51527551e-01
#cr=-3.46949797e+00

total_scme = np.zeros_like(total_mp2)
total_opt = np.zeros_like(total_mp2)

for i, a in enumerate(names):
    cluster = read('hexamers/'+a+'.xyz')
    cluster.cell = (25,25,25)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=True, 
                           te=te, 
                           td=td, 
                           Ar=Ar,
                           br=br,
                           cr=cr)
    total_scme[i] = cluster.get_potential_energy()

    
    #optclt = Tr('../scme_OUT_2/data/'+a+'-scme-new-opt.traj')
    #optcl = optclt[-1]

    cluster.calc = SCME_PS(cluster, 
                         numerical=False, 
                         te=te, 
                         td=td, 
                         Ar=Ar,
                         br=br,
                         cr=cr)
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj')
    dyn.run(fmax=0.001)
    total_opt[i] = cluster.get_potential_energy()


corr = np.array([-0.13, 0.05, 0.26, 0.26, 0.48, 0.26, 0.49, 0.47]) * kcal/mol
total_corr = total_mp2 * Hartree + corr


#dE_mp2  = (total_corr - np.min(total_corr)) 
#dE_scme = (total_scme - np.min(total_scme))
#dE_opt = (total_opt - np.min(total_opt))
dE_mp2  = (total_corr - total_corr[0]) #* mol / kcal
dE_scme = (total_scme - total_scme[0]) #* mol / kcal
dE_opt = (total_opt - total_opt[0]) #* mol / kcal

x = np.arange(0,len(names),1)
pl.plot(x, dE_mp2 * mol / kcal, 'r*', label='dCCSD')
#pl.plot(x, dE_scme * 23., 'b*', label='SCME')
pl.plot(x, dE_opt * mol / kcal, 'b*', label='SCME-rlx')
pl.plot(x, abs(dE_mp2-dE_opt)/6.,'g-',label='dE per mol')
pl.legend(loc='best')
pl.ylim([0, 4])
pl.show()
