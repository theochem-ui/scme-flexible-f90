# Paths and files //////////////////////////////////////////////////////
scanthese=src

root:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
root:=$(root:/=)
src=$(root)/src
bd:=$(root)/obj
link=$(bd)/*.o
vpath %.f90 $(src)



# Flags ////////////////////////////////////////////////////////////////
base:= -I$(bd) -J$(bd) 

spec:=-cpp -ffree-line-length-0 -fdefault-real-8 -fdefault-double-8  -fmax-array-constructor=300000  -fPIC

err:=  -fbacktrace -fmax-errors=3 -Wall -fcheck=all -Wno-unused-variable
# 
# -ffpe-trap=zero,overflow
#,underflow # kan inte ha denna för vid något avstånd blir siffrorna för små! underflow error
#-Wno-unused-variable

ana:= -g -pg 

opti:= 
#-march=native -Ofast -flto 
#-fopenmp
#-Og
#-Ofast -flto 
#-march=native -Ofast -flto -fopenmp
#-march=native -Og 
#-march=native -Ofast -flto -fexternal-blas -lblas -fopenmp
# -ftree-vectorize -ftree-loop-if-convert -ftree-loop-distribution -finline-functions 

fflags:= $(base) $(spec) $(err) $(ana) $(opti)

libs:= -lblas -llapack


# Compilation //////////////////////////////////////////////////////////
fc:=gfortran
f2c:=f2py

# Must place main object first for generic program to call the correct module: 
generic = -o $@ -DMODU=$(notdir $(<:.o=)) $(src)/generic_program.f08
comp    = $(fc) $(fflags) $(generic) $(link) $(libs)
f2py    = $(f2c) $(link) -m $@ -I$(bd) --f90flags="$(spec) $(err) $(ana) $(opti) $(libs)"

$(bd)/%.o: %.f90
	@mkdir -p $(bd)
	$(fc) -o $@ -c $< $(fflags)

