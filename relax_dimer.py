import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.visualize import view

n2a = read('../2Cs.xyz')
n3 = read('../3UUD.xyz')
n4 = read('../4Ci.xyz')
n5 = read('../5CAA.xyz')

p1 = n2a[:3]
p2 = n2a[3:]

for a in range(3):
        p2[a].x += 1.0

n2 = p1 + p2

n2.cell = (25,25,25)
n3.cell = (25,25,25)
n4.cell = (25,25,25)
n5.cell = (25,25,25)

# [ 1.00802672e+00  5.63580073e+00  1.38820856e+04 -2.99999975e+00
#  -2.71729004e+00]

te=1.13922136e+00  
td=8.50782754e+00  
Ar=7.70884702e+03 
br=-5.36242472e-01
cr=-3.48164066e+00

from ase.constraints import FixBondLengths

f = []
for i in range(len(n2)//3):
    f.append([i*3,i*3+1])
    f.append([i*3,i*3+2])
    f.append([i*3+1,i*3+2])
 
c = FixBondLengths(f)
n2.set_constraint(c)

#te=1.22120290e+00  
#td=4.99172506e+00  
#Ar=7.36603824e+03 
#br=-3.25662200e-01
#cr=-3.62777285e+00

# BEST SO FAR
#te=1.10454150e+00                                                                              
#td=7.55480010e+00  
#Ar=8.14963246e+03 
#br=-5.51527551e-01
#cr=-3.46949797e+00

n2.calc = SCME_PS(n2, 
                  te=te, 
                  td=td,
                  AO=Ar,
                  bO=br,
                  cO=cr,
                  flex_flag='none', numerical=False)

from ase.optimize import BFGS
dyn = BFGS(n2, trajectory='n2.traj')
dyn.run(fmax=0.001)
write('n2_opt.cube', n2)
