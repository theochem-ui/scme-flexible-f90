from ase.io.trajectory import Trajectory as Tr
from ase_interface_mpi import SCME_PS

sys = Tr('h2o.traj')
h2o = sys[-1]

h2o.calc = SCME_PS(h2o)

from ase.optimize import BFGS
opt = BFGS(h2o, trajectory='opt_h2o.traj')
opt.run(fmax=0.0001)

#h2o.rotate(33,'z')
#h2o.rotate(17,'x')
#h2o.rotate(-77,'y')

# Get equilibrium angles and distances
ang_eq = h2o.get_angle(1,0,2)
r_eq   = h2o.get_distance(0,1)
com = h2o.get_center_of_mass()

print(ang_eq,r_eq,com)

# Bisector and unit basis
V = com - h2o[1].position
C = com - h2o[2].position

import numpy as np
U = np.linalg.norm(V)*C + np.linalg.norm(C)*V

ez = U / np.linalg.norm(U)
Vez = np.dot(V,ez)

X = V - Vez * ez

ex = X / np.linalg.norm(X)

ey = np.cross(ez,ex)

# Place dummy atoms #
print((180.0 - ang_eq) / 2.0, ey)

rot = (360.0 - ang_eq) / 2.0

h2oA = h2o[1:2].copy()
h2oB = h2o[2:3].copy()

# Place along ez axis
h2oA.set_positions([ez*r_eq + com])
h2oB.set_positions([ez*r_eq + com])

from ase.visualize import view

view(h2o+h2oA+h2oB)

# Alternative
from math import cos, sin, pi

#
a = 106.45 * pi / 180. / 2.0
c = cos(a)
s = sin(a)
#
p = h2oA[0].position - com
v = ex / np.linalg.norm(ex)
new_pos_A = [(c * p - np.cross(p,s*v) + p.dot(v)*((1.-c)*v) + com)]

print(new_pos_A, new_pos_A[0] + 11.)
h2oA.set_positions(new_pos_A)

view(h2o+h2oA+h2oB)

# Second atom
X2 = C - C.dot(ez) * ez
ex2 = X2 / np.linalg.norm(X2)

#                            
a = 106.45 * pi / 180. / 2.0 
c = cos(a)                   
s = sin(a)                   
#                            
p = h2oB[0].position - com   
v = ex2 / np.linalg.norm(ex2)  
new_pos_B = (c * p - np.cross(p,s*v) + np.outer(p.dot(v),(1.-c)*v) + com)

h2oB.set_positions(new_pos_B)

view(h2o+h2oA+h2oB)
print(new_pos_B, new_pos_B[0] + 11.)
