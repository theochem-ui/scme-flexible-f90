from ase import Atoms
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
import numpy as np

# Read in previous
traj = Trajectory('tip3p_729mol_equil-B.traj')
atoms = traj[-1]

## RATTLE-type constraints on O-H1, O-H2, H1-H2.
#atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
#                                   for i in range(3**3)
#                                   for j in [0, 1, 2]], tolerance=1e-06)
#
#tag = 'tip3p_27mol_equil'
#atoms.calc = TIP3P(rc=4.5)
#md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
#              friction=0.01, logfile=tag + '.log')
#
#traj = Trajectory(tag + '-C.traj', 'w', atoms)
#md.attach(traj.write, interval=1)
#md.run(1000) # - then run large cell

# Repeat box and equilibrate further - 729 molecules
tag = 'tip3p_729mol_equil-C'
#atoms.set_constraint()  # repeat not compatible with FixBondLengths currently.
#atoms = atoms.repeat((3, 3, 3))
atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(len(atoms) // 3)
                                   for j in [0, 1, 2]], tolerance=1e-06)
atoms.calc = TIP3P(rc=7.)
md = Langevin(atoms, 2 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '.log')

traj = Trajectory(tag + '.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(2000)
