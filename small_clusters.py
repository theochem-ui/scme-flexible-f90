import numpy as np
import pylab as pl
from ase.units import Hartree, kcal, mol
from ase.io import read
from ase_interface_mpi import SCME_PS
from ase.optimize import BFGS, FIRE, MDMin
from ase.io.trajectory import Trajectory as Tr
from ase.constraints import FixAtoms
from ase.optimize.bfgslinesearch import BFGSLineSearch
#from ase.optimize.bfgslinesearch import BFGSLineSearch as BFGS

# Trimers
trimers = ['UUD', 'UUU']

energy_trimers = np.array([-229.136798, -229.135914]) * Hartree

corr_trimers = np.array([0.02, 0.08]) * kcal / mol

# Quadromers
quadromers = ['S4','Ci','Py']

energy_quadromers = np.array([-305.526378, -305.525027, -305.520274]) * Hartree

corr_quadromers = np.array([0.21, 0.22, -0.07]) * kcal / mol

# Pentamers
pentamers = ['CYC','FRB','CAC',
             'CAA','CAB','FRC',
             'FRA']

energy_pentamers = np.array([-381.910877, -381.908608, -381.908032,
                             -381.907815, -381.906536, -381.904564,
                             -381.905669]) * Hartree

corr_pentamers = np.array([0.37, 0.08, -0.10,
                           -0.08, -0.17, -0.02,
                           -0.02]) * kcal / mol

# Hexamers
hexamers = ['prism', 'cage', 'book-1', 
            'book-2','bag','cyclic-ring',
            'cyclic-boat-1','cyclic-boat-2']

energy_hexamers = np.array([-458.296817, -458.296753, -458.296426, 
                            -458.295839, -458.295335, -458.294962,
                            -458.293693, -458.293581]) * Hartree

corr_hexamers = np.array([-0.13, 0.05, 0.26, 
                           0.26, 0.48, 0.26, 
                           0.49, 0.47]) * kcal / mol

# Optimal ATM
te=1.10454150e+00                                                                               
td=7.55480010e+00  
AO=8.14963246e+03 
bO=-5.51527551e-01
cO=-3.46949797e+00

#te=1.10554147e+00  
#td=7.36752107e+00  
#AO=8.31224185e+03 
#bO=-5.49158335e-01
#cO=-3.46948973e+00

#te=1.10136983e+00  
#td=7.59377181e+00  
#AO=8.66539559e+03 
#bO=-5.11471327e-01
#cO=-3.50480806e+00

#te=1.10036499e+00
#td=7.09887127e+00
#AO=8.66033865e+03
#bO=-5.64462066e-01
#cO=-3.48562837e+00

#te=1.09442228e+00  
#td=7.56424049e+00 
#AO=8.71032621e+03 
#bO=-5.27481541e-01
#cO=-3.50213393e+00

#te=1.10022425e+00  
#td=7.58150783e+00  
#AO=8.57763291e+03 
#bO=-5.32461721e-01
#cO=-3.50001777e+00


#te=1.09984359e+00
#td=7.61857402e+00
#AO=8.57778344e+03
#bO=-5.34224485e-01
#cO=-3.50375235e+00

# SCME trimer
trimers_fixed = np.zeros_like(energy_trimers)
trimers_opt   = np.zeros_like(energy_trimers)
 
for i, a in enumerate(trimers):
    cluster = read('trimers/'+a+'.xyz')
    cluster.cell = (35,35,35)
    cluster.center()
 
    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)
 
    trimers_fixed[i] = cluster.get_potential_energy()
    
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj')
    dyn.run(fmax=0.001)
    trimers_opt[i] = cluster.get_potential_energy()
 
 
energy_trimers += corr_trimers
# Energy differences - hexamers
dE_cbs  = (energy_trimers - energy_trimers[0])
dE_fixed = (trimers_fixed - trimers_fixed[0])
dE_opt = (trimers_opt - trimers_opt[0])
 
x = np.arange(0,len(trimers),1)
pl.plot(x, dE_cbs * mol / kcal, 'k*', label='CCSD')
pl.plot(x, dE_cbs * mol / kcal, 'k--', label='CCSD')
pl.plot(x, dE_fixed * mol / kcal, 'b*', label='SCME')
pl.plot(x, dE_opt * mol / kcal, 'g*', label='SCME opt')
pl.plot(x, dE_opt * mol / kcal, 'g--', label='SCME opt')
pl.legend(loc='best')
pl.ylim([0, 4]) 
pl.show()

# SCME quadromers
quadromers_fixed = np.zeros_like(energy_quadromers)
quadromers_opt   = np.zeros_like(energy_quadromers)

for i, a in enumerate(quadromers):
    if a == 'Py':
        cluster = read('data/Py.cube')
        c = FixAtoms(indices=[atom.index for atom in cluster if atom.symbol == 'O'])
        cluster.set_constraint(c)
    else:
        cluster = read('quadromers/'+a+'.xyz')

    cluster.cell = (35,35,35)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)

    quadromers_fixed[i] = cluster.get_potential_energy()
    
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj')
    dyn.run(fmax=0.001)
    quadromers_opt[i] = cluster.get_potential_energy()


energy_quadromers += corr_quadromers
# Energy differences - hexamers
dE_cbs  = (energy_quadromers - energy_quadromers[0])
dE_fixed = (quadromers_fixed - quadromers_fixed[0])
dE_opt = (quadromers_opt - quadromers_opt[0])

x = np.arange(0,len(quadromers),1)
pl.plot(x, dE_cbs * mol / kcal, 'k*', label='CCSD')
pl.plot(x, dE_cbs * mol / kcal, 'k--', label='CCSD')
pl.plot(x, dE_fixed * mol / kcal, 'b*', label='SCME')
pl.plot(x, dE_opt * mol / kcal, 'g*', label='SCME opt')
pl.plot(x, dE_opt * mol / kcal, 'g--', label='SCME opt')
pl.legend(loc='best')
pl.ylim([0, 4])
pl.show()

# SCME pentamers
pentamers_fixed = np.zeros_like(energy_pentamers)
pentamers_opt   = np.zeros_like(energy_pentamers)

for i, a in enumerate(pentamers):
    cluster = read('pentamers/'+a+'.xyz')
    #if a == 'CAB':
    #    c = FixAtoms(indices=[atom.index for atom in cluster if atom.symbol == 'O'])
    #    cluster.set_constraint(c)
    cluster.cell = (35,35,35)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)

    pentamers_fixed[i] = cluster.get_potential_energy()
    
    #dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj', maxstep=0.01)
    dyn = MDMin(cluster, trajectory='data/'+a+'-scme-opt.traj', dt=0.01)
    #dyn = BFGSLineSearch(cluster, trajectory='data/'+a+'-scme-opt.traj')#, maxstep=0.01)
    #dyn.run(fmax=0.001)
    dyn.run(fmax=0.008)
    pentamers_opt[i] = cluster.get_potential_energy()


energy_pentamers += corr_pentamers
# Energy differences - hexamers
dE_cbs  = (energy_pentamers - energy_pentamers[0])
dE_fixed = (pentamers_fixed - pentamers_fixed[0])
dE_opt = (pentamers_opt - pentamers_opt[0])

x = np.arange(0,len(pentamers),1)
pl.plot(x, dE_cbs * mol / kcal, 'k*', label='CCSD')
pl.plot(x, dE_cbs * mol / kcal, 'k--', label='CCSD')
pl.plot(x, dE_fixed * mol / kcal, 'b*', label='SCME')
pl.plot(x, dE_opt * mol / kcal, 'g*', label='SCME opt')
pl.plot(x, dE_opt * mol / kcal, 'g--', label='SCME opt')
pl.legend(loc='best')
pl.ylim([0, 4])
pl.show()

# SCME hexamers
hexamers_fixed = np.zeros_like(energy_hexamers)
hexamers_opt   = np.zeros_like(energy_hexamers)

for i, a in enumerate(hexamers):
    cluster = read('hexamers/'+a+'.xyz')
    cluster.cell = (35,35,35)
    cluster.center()

    cluster.calc = SCME_PS(cluster, 
                           numerical=False, 
                           te=te, 
                           td=td, 
                           AO=AO,
                           bO=bO,
                           cO=cO)

    hexamers_fixed[i] = cluster.get_potential_energy()
    
    dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj')
    dyn.run(fmax=0.001)
    hexamers_opt[i] = cluster.get_potential_energy()


energy_hexamers += corr_hexamers
# Energy differences - hexamers
dE_cbs  = (energy_hexamers - energy_hexamers[0])
dE_fixed = (hexamers_fixed - hexamers_fixed[0])
dE_opt = (hexamers_opt - hexamers_opt[0])

x = np.arange(0,len(hexamers),1)
pl.plot(x, dE_cbs * mol / kcal, 'k*', label='CCSD')
pl.plot(x, dE_cbs * mol / kcal, 'k--', label='CCSD')
pl.plot(x, dE_fixed * mol / kcal, 'b*', label='SCME')
pl.plot(x, dE_opt * mol / kcal, 'g*', label='SCME opt')
pl.plot(x, dE_opt * mol / kcal, 'g--', label='SCME opt')
pl.legend(loc='best')
pl.ylim([0, 4])
pl.show()
