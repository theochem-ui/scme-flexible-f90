import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.visualize import view

te=1.20363264e+00  
td=5.36055971e+00  
Ar=9.63937695e+03 
br=-7.18544048e-01
cr=-3.55369543e+00

energy = []
volume = []

#a = ['1.024000','1.040000']
#a = ['0.992000','1.008000']
a = ['0.976000']

def re_index(sys):
    nM = 96
    # Grab first H2O
    sys_new = sys[nM*2:nM*2+1]
    sys_new += sys[0]
    sys_new += sys[1]

    for i in range(1,nM):
        sys_new += sys[nM*2+i]
        sys_new += sys[i*2]
        sys_new += sys[i*2+1]

    return sys_new

# Read in all relaxed ice-Ih structures
for val in a:
    traj = Tr('data/ice-qha-%s-expanded_B.traj' %val)
    ice = traj[-1]

    #cell = sys.cell.diagonal()
    #sys.cell = cell

    #sys_new = re_index(sys)

    #ice = sys_new.repeat((2,2,2))
    cell = ice.cell.diagonal()
    vol = cell[0]*cell[1]*cell[2]

    ice.calc = SCME_PS(ice,
                   numerical=False,
                   static_flag='dqoh', # <--- FLAG to turn on/off static moments
                   ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                   flex_flag='dq',
                   te=te,
                   td=td,
                   Ar=Ar,
                   br=br,
                   cr=cr)

    dyn = BFGS(ice, trajectory='data/ice-qha-%s-expanded_C.traj' %val)
    dyn.run(fmax=0.01)
    e = ice.get_potential_energy()

    energy.append(e/8./96.)
    volume.append(vol/8./96.)

import pylab as pl
e = np.array(energy)
v = np.array(volume)

from ase.eos import EquationOfState
eos = EquationOfState(v, e)
from ase.units import kJ
                   
v0, e0, B = eos.fit()
print(v0,e0,B)  
print(B / kJ * 1.0e24, 'GPa')

