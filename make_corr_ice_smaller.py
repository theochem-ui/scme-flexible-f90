import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.visualize import view

te=1.10454150e+00                                                                               
td=7.55480010e+00
Ar=8.14963246e+03
br=-5.51527551e-01
cr=-3.46949797e+00


energy = []
volume = []

#a = ['1.024000','1.040000']
#a = ['0.992000','1.008000']

# Read in previous ice
traj = Tr('data/ice-qha-scme-1.072000.traj')
ice = traj[-1]

scaled = ice.get_scaled_positions()

new_dim = 1.072000**(1./3.)

ice.set_cell((4.4969*6.0*new_dim,
              7.7889*4.0*new_dim,
              7.3211*4.0*new_dim), scale_atoms=True)

# Read in all relaxed ice-Ih structures
cell = ice.cell.diagonal()
vol = cell[0]*cell[1]*cell[2]

ice.calc = SCME_PS(ice,
               numerical=False,
               static_flag='dqoh', # <--- FLAG to turn on/off static moments
               ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
               flex_flag='dq',
               te=te,
               td=td,
               Ar=Ar,
               br=br,
               cr=cr)

dyn = BFGS(ice, trajectory='data/ice-qha-scme-1.072000-NEW.traj')
dyn.run(fmax=0.01)
e = ice.get_potential_energy()

energy.append(e/8./96.)
volume.append(vol/8./96.)
