import time

import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.visualize import view
from ase.constraints import FixBondLengths, FixAtoms
from ase.optimize import BFGS

te=1.10036499e+00
td=7.09887127e+00
Ar=8.66033865e+03
br=-5.64462066e-01
cr=-3.48562837e+00

T1 = time.time()

data_c1  = open('dimer_c1.data', 'w')
data_ci  = open('dimer_ci.data', 'w')
data_c2v = open('dimer_c2v.data', 'w')

# 
npts = 74
dimers = ['C1','Ci','C2v']

e_c1  = []
e_ci  = []
e_c2v = []

d_c1  = []
d_ci  = []
d_c2v = []

from ase.units import kcal, mol

for a in dimers:
    eq = read('stationary_dimers/'+a+'.xyz')
    pos = eq.positions.copy()
    if a == 'C1':
        pos[3:,1] -= 12.0 * 0.05
        eq.set_positions(pos)

    elif a == 'Ci':
        pos[3:,0] -= 12.0 * 0.05
        eq.set_positions(pos)

    else:
        pos[3:,2] += 12.0 * 0.05
        eq.set_positions(pos)

    eq.center(vacuum=20)

    for j in range(npts):
        d = eq.get_distance(0,3)
        eq.calc = SCME_PS(eq,
                          numerical=False,
                          te=te,
                          td=td,
                          AO=Ar,
                          bO=br,
                          cO=cr)
        e = eq.get_potential_energy()

        if a == 'C1':
            d_c1.append(d)
            e_c1.append(e)
            write('C1_total.xyz', eq, comment=e * mol / kcal, append=True)
            pos = eq.positions.copy()
            # Move A and B by 0.025 along y-axis
            pos[:3,1] -= 0.025
            pos[3:,1] += 0.025
            eq.set_positions(pos)

        elif a == 'Ci':
            d_ci.append(d)
            e_ci.append(e)
            write('Ci_total.xyz', eq, comment=e * mol / kcal, append=True)
            pos = eq.positions.copy()
            # Move A and B by 0.025 along y-axis
            pos[:3,0] -= 0.025
            pos[3:,0] += 0.025
            eq.set_positions(pos)
        
        else:
            d_c2v.append(d)
            e_c2v.append(e)
            write('C2v_total.xyz', eq, comment=e * mol / kcal, append=True)
            pos = eq.positions.copy()
            # Move A and B by 0.025 along y-axis
            pos[:3,2] += 0.025
            pos[3:,2] -= 0.025
            eq.set_positions(pos)


import pylab as pl
pl.plot(d_c1, e_c1, 'r--')
pl.plot(d_ci, e_ci, 'b--')
pl.plot(d_c2v, e_c2v, 'g--')
pl.show()

