from ase.io.trajectory import Trajectory as Tr

traj = Tr('h2o.traj')
h2o = traj[-1]

h2o.set_positions(h2o.positions - h2o.positions[0])

pos = h2o.positions.copy()

#pos[:,2] -= pos[0,2]

print(pos)
qh = 0.330970884525

import numpy as np

Q = np.zeros((3,3))
Q[0,0] = qh * (2 * pos[1,0]**2 - pos[1,2]**2 + pos[0,2]**2)
Q[1,1] = qh * (-pos[1,0]**2 - pos[1,2]**2 + pos[0,2]**2)
Q[2,2] = qh * (-pos[1,0]**2 + 2*pos[1,2]**2 - 2*pos[0,2]**2)

print(Q)

zm2 = pos[1,2]**2 - pos[1,0]**2 / 2.
print(zm2, np.sqrt(zm2))

Qzz = qh * (-pos[1,0]**2 + 2*pos[1,2]**2 - 2*zm2)
print(Qzz)

y = - 2 * np.sqrt(zm2) / (pos[1,2] + pos[2,2])
print(y)

Qt = np.zeros((3,3))
pos_z = pos.copy()

z_m = pos[0] + y / 2. * (pos[1] + pos[2] - 2 * pos[0])

pos_z[0] += z_m
Qt[0,0] = qh * (2 * pos_z[1,0]**2 - pos_z[1,2]**2 + pos_z[0,2]**2)
Qt[1,1] = qh * (-pos_z[1,0]**2 - pos_z[1,2]**2 + pos_z[0,2]**2)
Qt[2,2] = qh * (-pos_z[1,0]**2 + 2*pos_z[1,2]**2 - 2*pos_z[0,2]**2)

print(z_m)
print(Qt)

Q_new = np.zeros((3,3))

cm = h2o.get_center_of_mass()

pos_cm = pos - cm

print(pos)

dO = pos_cm[0]
#dO  = z_m - cm #pos_cm[0] + y / 2. * (pos_cm[1] + pos_cm[2] - 2 * pos_cm[0])
dH1 = pos_cm[1] #- cm
dH2 = pos_cm[2] #- cm

qO = -2*qh

for i in range(3):
    for j in range(3):
        Q_new[i,j] += 3.0 * qO * dO[i] * dO[j]
        Q_new[i,j] += 3.0 * qh * dH1[i] * dH1[j]
        Q_new[i,j] += 3.0 * qh * dH2[i] * dH2[j]
        if i == j:
            Q_new[i,j] -= qO * (dO**2).sum()
            Q_new[i,j] -= qh * (dH1**2).sum()
            Q_new[i,j] -= qh * (dH2**2).sum()

Q_new *= 0.5

print(Q_new)

dip = np.zeros(3)
for i in range(3):
    dip[i] += qO / (1 - y) * (z_m - cm)[i]
    dip[i] += (qh - y / 2 / (1 - y) * qO) * pos_cm[1,i]
    dip[i] += (qh - y / 2 / (1 - y) * qO) * pos_cm[2,i]


from ase.units import Bohr, Debye
print(Debye)
print(dip / Bohr)
print(Q_new / Debye)

