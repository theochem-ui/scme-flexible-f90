import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.constraints import FixAtoms

# LATEST and BEST SET OF PARAMETERS
te=1.10454150e+00                                                                           
td=7.55480010e+00  
Ar=8.14963246e+03 
br=-5.51527551e-01
cr=-3.46949797e+00

dimer_energies = np.zeros(22)
data = open('dimer_curve_target.data', 'r')
lines = data.readlines()

for i, line in enumerate(lines):
    dimer_energies[i] = float(line.split()[1])

print(dimer_energies)

data.close()

def get_bulk_prop(x):
    energy = []
    volume = []
    
    a = ['0.856000','0.880000','0.904000','0.928000',
         '0.952000','0.976000','1.000000','1.024000',
         '1.048000','1.072000']
    
    # Read in all relaxed ice-Ih structures
    for i, val in enumerate(a):
        traj = Tr('data/ice-scme-%s-new.traj' %val)
        ice = traj[-1]
    
        #ice = sys.repeat((2,2,2))
        #cell = ice.cell.diagonal()
        #vol = cell[0]*cell[1]*cell[2]
    
        ice.calc = SCME_PS(ice, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])

        e = ice.get_potential_energy()

        energy.append(e/8.)
   
    energy = np.array(energy)

    energy_o=np.array([
                       -0.62891008, -0.636716,   -0.64184079, 
                       -0.64444613, -0.64509349, -0.64391332,
                       -0.6412161,  -0.6372757,  -0.63229588, -0.62641337])

    return energy/96.0, energy_o


def get_dimer(x):
    # Read in cube - fix oxygen distance, relax, get energy
    e_dimer = np.zeros(22)
    for i in range(22):
        dimer = read('dimer/water_dimer_%d_target.cube' %i)
        c = FixAtoms(indices=[atom.index for atom in dimer if atom.symbol == 'O'])
        dimer.set_constraint(c)
        dimer.calc = SCME_PS(dimer, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
        dyn = BFGS(dimer, trajectory='data/dimer_%d.traj' %i, 
                   logfile='data/dimer_%d.log' %i)
        dyn.run(fmax=0.001)
        e_dimer[i] = dimer.get_potential_energy()

    return e_dimer/2., dimer_energies/2.


def get_small(x):
    n3 = read('../3UUD.xyz')
    n4 = read('../4Ci.xyz')
    n5 = read('../5CAA.xyz')

    n6c = read('../scme_ff_optimized/hexamers/cage.xyz')

    n3.cell  = (25,25,25)
    n4.cell  = (25,25,25)
    n5.cell  = (25,25,25)
    n6c.cell = (25,25,25)

    n3.center()
    n4.center()
    n5.center()
    n6c.center()

    n3.calc = SCME_PS(n3, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
    dyn = BFGS(n3, trajectory='data/n3.traj', logfile='data/n3.log')
    dyn.run(fmax=0.001)
    e3 = n3.get_potential_energy()

    n4.calc = SCME_PS(n4, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
    dyn = BFGS(n4, trajectory='data/n4.traj', logfile='data/n4.log')
    dyn.run(fmax=0.001)
    e4 = n4.get_potential_energy()

    n5.calc = SCME_PS(n5, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
    dyn = BFGS(n5, trajectory='data/n5.traj', logfile='data/n5.log')
    dyn.run(fmax=0.01)
    e5 = n5.get_potential_energy()

    n6c.calc = SCME_PS(n6c, numerical=False, 
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
    dyn = BFGS(n6c, trajectory='data/n6c.traj', logfile='data/n6c.log')
    dyn.run(fmax=0.001)
    e6c = n6c.get_potential_energy()


    eo3 = -0.685153
    eo4 = -1.19685
    eo5 = -1.57413
    eo6c = -1.985643
    n = np.array([eo3/3.,eo4/4.,eo5/5.,eo6c/6.])
    m = np.array([e3/3.,e4/4.,e5/5.,e6c/6.])
    return m, n


def func(p):

    k, j = get_dimer(p)
    x, y = get_bulk_prop(p)
    m, n = get_small(p)

    ffa = np.append(k,x)
    tta = np.append(j,y)

    ff = np.append(ffa,m)
    tt = np.append(tta,n)

    print(np.sqrt(1.0/len(ff)*((ff-tt)**2).sum()))
    return ff - tt

from scipy.optimize import minimize, least_squares

bounds = np.array([[1.0, 3.0], 
                   [2.0, 9.0], 
                   [0.5e+04, 6.0e+04],
                   [-6.0, -0.25],
                   [-6.0, -0.5]])

res = least_squares(func, 
                    np.array([te, td, Ar, br, cr]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=1e-10, xtol=1e-10, 
                    max_nfev=100, verbose=1)
print(res.x)
