import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.constraints import FixAtoms
from ase.units import Hartree, kcal, mol

# LATEST and BEST SET OF PARAMETERS
# BEST SET SO FAR
#te=1.10454150e+00                                                                           
#td=7.55480010e+00  
#Ar=8.14963246e+03 
#br=-5.51527551e-01
#cr=-3.46949797e+00

#te=1.10529215e+00  
#td=7.35498880e+00  
#Ar=8.14513544e+03 
#br=-5.53568613e-01
#cr=-3.46380532e+00

#te=1.10488184e+00  
#td=7.31779893e+00  
#Ar=8.32917452e+03 
#br=-5.49383923e-01
#cr=-3.47823126e+00

#te=1.10488184e+00
#td=7.31779893e+00
#AO=8.32917452e+03
#bO=-5.49383923e-01
#cO=-3.47823126e+00

#te=1.10554147e+00
#td=7.36752107e+00 
#AO=8.31224185e+03 
#bO=-5.49158335e-01
#cO=-3.46948973e+00

te=1.10136983e+00
td=7.59377181e+00  
AO=8.66539559e+03 
bO=-5.11471327e-01
cO=-3.50480806e+00

dimer_energies = np.zeros(32)
data = open('dimer_curve_target.data', 'r')
lines = data.readlines()

for i, line in enumerate(lines):
    dimer_energies[i] = float(line.split()[1])

data.close()


def get_bulk_prop(x):
    energy = []
    volume = []
    
    a = ['0.856000','0.880000','0.904000','0.928000',
         '0.952000','0.976000','1.000000','1.024000',
         '1.048000','1.072000']
    
    # Read in all relaxed ice-Ih structures
    for i, val in enumerate(a):
        traj = Tr('data/ice-qha-scme-%s-NEW.traj' %val)
        ice = traj[-1]
    
        #ice = sys.repeat((2,2,2))
        #cell = ice.cell.diagonal()
        #vol = cell[0]*cell[1]*cell[2]
    
        ice.calc = SCME_PS(ice, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])

        e = ice.get_potential_energy()

        energy.append(e/8.)
   
    energy = np.array(energy)

    #energy_o = np.array([-0.62822578, -0.63646103, -0.64197978,
    #                     -0.64490041, -0.64581724, -0.64485744,
    #                     -0.64233742, -0.6385353,  -0.6336691,  -0.62787326])

    # NEW
    #energy_o = np.array([-0.62141164,-0.63130136,-0.63859689,
    #                     -0.64303422,-0.64544599,-0.64585364,
    #                     -0.64459449,-0.64193581,-0.63812961,-0.63332233])

    # Almost perfect crystal ICE-Ih
    #energy_o = np.array([-0.60766745,-0.61392605,-0.61913705,
    #                     -0.62156954,-0.6228687,-0.62247533,
    #                     -0.62076236,-0.61779466,-0.61386144,-0.60914826])

    energy_o = np.array([-0.64110721,-0.64861388, -0.65340198, 
                         -0.65580065, -0.65614991, -0.65474443,
                         -0.65184955, -0.6476879,  -0.6425237,  -0.63649365])

    return energy/96.0, energy_o


def get_dimer(x):
    # Read in cube - fix oxygen distance, relax, get energy
    e_dimer = np.zeros(32)
    for i in range(32):
        dimer = read('dimer/water_dimer_%d_target.cube' %i)
        c = FixAtoms(indices=[atom.index for atom in dimer if atom.symbol == 'O'])
        dimer.set_constraint(c)
        dimer.calc = SCME_PS(dimer, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       AO=x[2], 
                       bO=x[3], 
                       cO=x[4])
        dyn = BFGS(dimer, trajectory='data/dimer_%d.traj' %i, 
                   logfile='data/dimer_%d.log' %i)
        dyn.run(fmax=0.001)
        e_dimer[i] = dimer.get_potential_energy()

    return e_dimer/2., dimer_energies/2.


def get_small(x):
    # Trimers
    # Trimers
    trimers = ['UUD', 'UUU']

    energy_trimers = np.array([-229.136798, -229.135914]) * Hartree
    corr_trimers = np.array([0.02, 0.08]) * kcal / mol
    trimers_opt = np.zeros_like(energy_trimers)
     
    for i, a in enumerate(trimers):
        cluster = read('trimers/'+a+'.xyz')
        cluster.cell = (35,35,35)
        cluster.center()
     
        cluster.calc = SCME_PS(cluster, 
                               numerical=False, 
                               te=x[0], 
                               td=x[1], 
                               AO=x[2],
                               bO=x[3],
                               cO=x[4])
        
        dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj',
                   logfile='data/'+a+'-scme-opt.log')
        dyn.run(fmax=0.001)
        trimers_opt[i] = cluster.get_potential_energy()
     
    energy_trimers += corr_trimers
    dEt_cbs = energy_trimers[1:] - energy_trimers[0]
    dEt_opt = trimers_opt[1:] - trimers_opt[0]



    # Quadromers
    quadromers = ['S4','Ci','Py']
     
    energy_quadromers = np.array([-305.526378, -305.525027, -305.520274]) * Hartree             
    corr_quadromers = np.array([0.21, 0.22, -0.07]) * kcal / mol
    quadromers_opt   = np.zeros_like(energy_quadromers)

    for i, a in enumerate(quadromers):
        if a == 'Py':
            cluster = read('data/Py.cube')
            c = FixAtoms(indices=[atom.index for atom in cluster if atom.symbol == 'O'])
            cluster.set_constraint(c)
        else:
            cluster = read('quadromers/'+a+'.xyz')
        cluster.cell = (35,35,35)
        cluster.center()
     
        cluster.calc = SCME_PS(cluster, 
                               numerical=False, 
                               te=x[0], 
                               td=x[1], 
                               AO=x[2],
                               bO=x[3],
                               cO=x[4])
        
        dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj',
                   logfile='data/'+a+'-scme-opt.log')
        dyn.run(fmax=0.001)
        quadromers_opt[i] = cluster.get_potential_energy()
     
    energy_quadromers += corr_quadromers
    # Energy differences - hexamers
    dEq_cbs  = (energy_quadromers[1:] - energy_quadromers[0])
    dEq_opt = (quadromers_opt[1:] - quadromers_opt[0])

    # Combine
    dE_cbs = np.append(dEt_cbs, dEq_cbs)
    dE_opt = np.append(dEt_opt, dEq_opt)

    # Pentamers
    pentamers = ['CYC','FRB','CAC',
                 'CAA','CAB','FRC',
                 'FRA']
     
    energy_pentamers = np.array([-381.910877, -381.908608, -381.908032,
                                 -381.907815, -381.906536, -381.904564,
                                 -381.905669]) * Hartree
     
    corr_pentamers = np.array([0.37, 0.08, -0.10,
                               -0.08, -0.17, -0.02,
                               -0.02]) * kcal / mol
    pentamers_opt   = np.zeros_like(energy_pentamers)
     
    for i, a in enumerate(pentamers):
        cluster = read('pentamers/'+a+'.xyz')
        if a == 'CAB':
            c = FixAtoms(indices=[atom.index for atom in cluster if atom.symbol == 'O'])
            cluster.set_constraint(c)
        cluster.cell = (35,35,35)
        cluster.center()
     
        cluster.calc = SCME_PS(cluster, 
                               numerical=False, 
                               te=x[0], 
                               td=x[1], 
                               AO=x[2],
                               bO=x[3],
                               cO=x[4])
     
        
        dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj',
                   logfile='data/'+a+'-scme-opt.log')
        dyn.run(fmax=0.001)
        pentamers_opt[i] = cluster.get_potential_energy()
     
    energy_pentamers += corr_pentamers
    # Energy differences - hexamers
    dEp_cbs  = (energy_pentamers[1:] - energy_pentamers[0])
    dEp_opt = (pentamers_opt[1:] - pentamers_opt[0])

    # Combine
    dE_cbs = np.append(dE_cbs, dEp_cbs)
    dE_opt = np.append(dE_opt, dEp_opt)

    # SCME hexamers
    hexamers = ['prism', 'cage', 'book-1', 
                'book-2','bag','cyclic-ring',
                'cyclic-boat-1','cyclic-boat-2']
     
    energy_hexamers = np.array([-458.296817, -458.296753, -458.296426, 
                                -458.295839, -458.295335, -458.294962,
                                -458.293693, -458.293581]) * Hartree
    corr_hexamers = np.array([-0.13, 0.05, 0.26, 
                               0.26, 0.48, 0.26, 
                               0.49, 0.47]) * kcal / mol
    hexamers_opt   = np.zeros_like(energy_hexamers)
     
    for i, a in enumerate(hexamers):
        cluster = read('hexamers/'+a+'.xyz')
        cluster.cell = (35,35,35)
        cluster.center()
     
        cluster.calc = SCME_PS(cluster, 
                               numerical=False, 
                               te=x[0], 
                               td=x[1], 
                               AO=x[2],
                               bO=x[3],
                               cO=x[4])
        
        dyn = BFGS(cluster, trajectory='data/'+a+'-scme-opt.traj',
                   logfile='data/'+a+'-scme-opt.log')
        dyn.run(fmax=0.001)
        hexamers_opt[i] = cluster.get_potential_energy()
     
    energy_hexamers += corr_hexamers
    # Energy differences - hexamers
    dEh_cbs  = (energy_hexamers[1:] - energy_hexamers[0])
    dEh_opt = (hexamers_opt[1:] - hexamers_opt[0])

    # combine and return
    dE_cbs = np.append(dE_cbs, dEh_cbs)
    dE_opt = np.append(dE_opt, dEh_opt)

    return dE_opt, dE_cbs


def func(p):
    m, n = get_small(p)
    k, j = get_dimer(p)
    x, y = get_bulk_prop(p)

    ffa = np.append(k,x)
    tta = np.append(j,y)

    ff = np.append(ffa,m)
    tt = np.append(tta,n)

    print(np.sqrt(1.0/len(ff)*((ff-tt)**2).sum()))
    return ff - tt

from scipy.optimize import minimize, least_squares

bounds = np.array([[1.0, 3.0], 
                   [2.0, 9.0], 
                   [0.5e+04, 6.0e+04],
                   [-6.0, -0.25],
                   [-6.0, -0.5]])

res = least_squares(func, 
                    np.array([te, td, AO, bO, cO]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=1e-10, xtol=1e-10, 
                    max_nfev=100, verbose=1)
print(res.x)
