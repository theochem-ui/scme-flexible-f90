import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS

t_e = 1.10
t_d = 4.40
Ar  = 18669.17400
br  = -1.4435
cr  = -3.46581

def get_bulk_prop(x):
    energy = []
    volume = []
    
    a = ['0.976000','0.992000','1.008000','1.024000','1.040000']
    
    # Read in all relaxed ice-Ih structures
    for val in a:
        traj = Tr('data/ice-qha-%s-expanded.traj' %val)
        ice = traj[-1]
    
        #ice = sys.repeat((2,2,2))
        cell = ice.cell.diagonal()
        vol = cell[0]*cell[1]*cell[2]
    
        ice.calc = SCME_PS(ice, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])

        #dyn = BFGS(ice, trajectory='data/ice-qha-%s-expanded.traj' %val)
        #dyn.run(fmax=0.01)
        e = ice.get_potential_energy()

        energy.append(e/8.)
        volume.append(vol/8.)
    
    energy = np.array(energy)
    volume = np.array(volume)
    
    data = open('e-v-2.dat')
    lines = data.readlines()
    
    volume_o = []
    energy_o = []
    
    for line in lines:
        energy_o.append(float(line.split()[1]))
        volume_o.append(float(line.split()[0]))
    
    energy_o = np.array(energy_o)
    volume_o = np.array(volume_o)

    return energy/96.0, energy_o/96.0

def get_small(x):
    n2t = Tr('data/n2.traj')
    n3t = Tr('data/n3.traj')
    n4t = Tr('data/n4.traj')
    n5t = Tr('data/n5.traj')
    n6t = Tr('data/n6c.traj')

    n2 = n2t[-1]
    n3 = n3t[-1]
    n4 = n4t[-1]
    n5 = n5t[-1]
    n6c = n6t[-1]

    n2.calc = SCME_PS(n2, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n2, trajectory='data/n2.traj', logfile='data/n2.log')
    dyn.run(fmax=0.001)
    e2 = n2.get_potential_energy()

    n3.calc = SCME_PS(n3, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n3, trajectory='data/n3.traj', logfile='data/n3.log')
    dyn.run(fmax=0.001)
    e3 = n3.get_potential_energy()

    n4.calc = SCME_PS(n4, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n4, trajectory='data/n4.traj', logfile='data/n4.log')
    dyn.run(fmax=0.001)
    e4 = n4.get_potential_energy()

    n5.calc = SCME_PS(n5, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n5, trajectory='data/n5.traj', logfile='data/n5.log')
    dyn.run(fmax=0.001)
    e5 = n5.get_potential_energy()

    n6c.calc = SCME_PS(n6c, numerical=False, 
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n6c, trajectory='data/n6c.traj', logfile='data/n6c.log')
    dyn.run(fmax=0.001)
    e6c = n6c.get_potential_energy()


    eo2 = -0.218953
    eo3 = -0.685153
    eo4 = -1.19685
    eo5 = -1.57413
    eo6c = -1.985643
    n = np.array([eo2,eo3,eo4,eo5,eo6c])
    m = np.array([e2,e3,e4,e5,e6c])
    return m, n
    #return abs(e + 0.22000)


def func(p):

    #x, y = get_bulk_prop(p)
    m, n = get_small(p)

    #ff = np.array([m[0]/2.0,x[0],x[1],x[2],x[4],x[3],m[1]/3.0,m[2]/4.0,m[3]/5.0,m[4]/6.0])
    #tt = np.array([n[0]/2.0,y[0],y[1],y[2],y[4],y[3],n[1]/3.0,n[2]/4.0,n[3]/5.0,n[4]/6.0])
    ff = np.array([m[0]/2.0,m[1]/3.0,m[2]/4.0,m[3]/5.0,m[4]/6.0])
    tt = np.array([n[0]/2.0,n[1]/3.0,n[2]/4.0,n[3]/5.0,n[4]/6.0])

    print((ff-tt).sum(), ff-tt)
    return ff - tt

from scipy.optimize import minimize, least_squares

bounds = np.array([[0.2, 1.7], 
                   [3.0, 5.0], 
                   [1.0e+04, 4.0e+04],
                   [-2.0, -1.0],
                   [-4.0, -2.0]])

res = least_squares(func, 
                    np.array([t_e, t_d, Ar, br, cr]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=1e-10, xtol=1e-10, 
                    max_nfev=100, verbose=1)
print(res.x)
