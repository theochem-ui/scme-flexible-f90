import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.constraints import FixAtoms

# LATEST SET OF PARAMETERS
te=1.18548257e+00  
td=7.00000000e+00  
Ar=1.00000000e+04 
br=-5.00000000e-01
cr=-3.57931155e+00

def get_bulk_prop(x):
    energy = []
    volume = []
    
    a = ['0.976000','0.992000','1.008000','1.024000','1.040000']
    
    # Read in all relaxed ice-Ih structures
    for i, val in enumerate(a):
        traj = Tr('../scme_ff_optimized/data/ice-qha-%s-.traj' %val)
        sys = traj[-1]
    
        ice = sys.repeat((2,2,2))
        cell = ice.cell.diagonal()
        vol = cell[0]*cell[1]*cell[2]
    
        ice.calc = SCME_PS(ice, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])

        e = ice.get_potential_energy()

        energy.append(e/8.)
        volume.append(vol/8.)
    
    energy = np.array(energy)
    volume = np.array(volume)
    
    data = open('e-v-2.dat')
    lines = data.readlines()
    
    volume_o = []
    energy_o = []
    
    for line in lines:
        energy_o.append(float(line.split()[1]))
        volume_o.append(float(line.split()[0]))
    
    energy_o = np.array(energy_o)
    volume_o = np.array(volume_o)

    return energy/96.0, energy_o/96.0

def get_dimer(x):
    # Read in cube - fix oxygen distance, relax, get energy
    e_dimer = np.zeros(11)
    for i, a in enumerate([0, 3, 6, 9, 14, 17, 20, 22, 25, 28, 31]):
        dimer = read('../scme_ff_optimized/dimer/water_dimer_%d_B.cube' %a)
        c = FixAtoms(indices=[atom.index for atom in dimer if atom.symbol == 'O'])
        dimer.set_constraint(c)
        dimer.calc = SCME_PS(dimer, 
                       numerical=False,
                       static_flag='dqoh', # <--- FLAG to turn on/off static moments
                       ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                       flex_flag='dq',
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
        dyn = BFGS(dimer, trajectory='data/dimer_%d.traj' %a, logfile='data/dimer_%d.log' %a)
        dyn.run(fmax=0.001)
        e_dimer[i] = dimer.get_potential_energy()

    e_obj = np.array([-0.0530122,  #1  
                      -0.1316733,  #4  
                      -0.1793656,  #7  
                      -0.2056548,  #10 
                      -0.219525,   #15 
                      -0.21696242, #18 
                      -0.20974957, #21 
                      -0.2033157,  #23 
                      -0.192244265, #26 
                      -0.18032639,#29
                      -0.168209676,#32
                      ])

    return e_dimer, e_obj


def get_small(x):
    n3 = read('../3UUD.xyz')
    n4 = read('../4Ci.xyz')
    n5 = read('../5CAA.xyz')

    n6c = read('../scme_ff_optimized/hexamers/cage.xyz')

    n3.cell  = (25,25,25)
    n4.cell  = (25,25,25)
    n5.cell  = (25,25,25)
    n6c.cell = (25,25,25)

    n3.calc = SCME_PS(n3, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n3, trajectory='data/n3.traj', logfile='data/n3.log')
    dyn.run(fmax=0.001)
    e3 = n3.get_potential_energy()

    n4.calc = SCME_PS(n4, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n4, trajectory='data/n4.traj', logfile='data/n4.log')
    dyn.run(fmax=0.001)
    e4 = n4.get_potential_energy()

    n5.calc = SCME_PS(n5, numerical=False,
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n5, trajectory='data/n5.traj', logfile='data/n5.log')
    dyn.run(fmax=0.001)
    e5 = n5.get_potential_energy()

    n6c.calc = SCME_PS(n6c, numerical=False, 
                       te=x[0], 
                       td=x[1], 
                       Ar=x[2], 
                       br=x[3], 
                       cr=x[4])
    dyn = BFGS(n6c, trajectory='data/n6c.traj', logfile='data/n6c.log')
    dyn.run(fmax=0.001)
    e6c = n6c.get_potential_energy()


    eo3 = -0.685153
    eo4 = -1.19685
    eo5 = -1.57413
    eo6c = -1.985643
    n = np.array([eo3,eo4,eo5,eo6c])
    m = np.array([e3,e4,e5,e6c])
    return m, n


def func(p):

    k, j = get_dimer(p)
    m, n = get_small(p)

    ff = np.array([
                   k[0]/2.,k[1]/2.,k[2]/2.,k[3]/2.,k[4]/2.,
                   k[5]/2.,k[6]/2.,k[7]/2.,k[8]/2.,k[9]/2.,k[10]/2.,
                   m[0]/3.,m[1]/4.,m[2]/5.,m[3]/6.])
    tt = np.array([
                   j[0]/2.,j[1]/2.,j[2]/2.,j[3]/2.,j[4]/2.,
                   j[5]/2.,j[6]/2.,j[7]/2.,j[8]/2.,j[9]/2.,j[10]/2.,
                   n[0]/3.,n[1]/4.,n[2]/5.,n[3]/6.])

    print(0.5 * ((ff-tt)**2).sum(), ff-tt)
    return ff - tt

from scipy.optimize import minimize, least_squares

bounds = np.array([[1.0, 2.0], 
                   [4.0, 9.0], 
                   [0.5e+04, 4.0e+04],
                   [-4.0, -0.25],
                   [-4.0, -0.5]])

res = least_squares(func, 
                    np.array([te, td, Ar, br, cr]), 
                    bounds=([b[0] for b in bounds],[b[1] for b in bounds]),
                    method='trf', 
                    ftol=1e-10, gtol=1e-10, xtol=1e-10, 
                    max_nfev=100, verbose=1)
print(res.x)
