import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr

traj = Tr('relax_from_mbpol.traj')
pice = traj[0]

#pice = ice.repeat((2,2,2))

pice.calc = SCME_PS(pice, numerical=True, repeat=(2,2,2), fix_oxygen=False, parallel=True)

print(pice.get_potential_energy())
