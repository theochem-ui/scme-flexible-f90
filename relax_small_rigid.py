import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.visualize import view
from ase.constraints import FixBondLengths

n2 = read('../2Cs.xyz')
n3 = read('../3UUD.xyz')
n4 = read('../4Ci.xyz')
n5 = read('../5CAA.xyz')

n2.cell = (25,25,25)
n3.cell = (25,25,25)
n4.cell = (25,25,25)
n5.cell = (25,25,25)

f = []
for i in range(len(n2)//3):
    f.append([i*3,i*3+1])
    f.append([i*3,i*3+2])
    f.append([i*3+1,i*3+2])
 
c = FixBondLengths(f)
n2.set_constraint(c)

f = []
for i in range(len(n3)//3):
    f.append([i*3,i*3+1])
    f.append([i*3,i*3+2])
    f.append([i*3+1,i*3+2])
 
c = FixBondLengths(f)
n3.set_constraint(c)

f = []
for i in range(len(n4)//3):
    f.append([i*3,i*3+1])
    f.append([i*3,i*3+2])
    f.append([i*3+1,i*3+2])
 
c = FixBondLengths(f)
n4.set_constraint(c)

f = []
for i in range(len(n5)//3):
    f.append([i*3,i*3+1])
    f.append([i*3,i*3+2])
    f.append([i*3+1,i*3+2])
 
c = FixBondLengths(f)
n5.set_constraint(c)

n2.calc = SCME_PS(n2, numerical=False)
n3.calc = SCME_PS(n3, numerical=False)
n4.calc = SCME_PS(n4, numerical=False)
n5.calc = SCME_PS(n5, numerical=False)

from ase.optimize import BFGS
dyn = BFGS(n2, trajectory='n2.traj')
dyn.run(fmax=0.01)
write('n2_opt_rigid.cube', n2)

dyn = BFGS(n3, trajectory='n3.traj')
dyn.run(fmax=0.01)
write('n3_opt_rigid.cube', n3)

dyn = BFGS(n4, trajectory='n4.traj')
dyn.run(fmax=0.01)
write('n4_opt_rigid.cube', n4)

dyn = BFGS(n5, trajectory='n5.traj')
dyn.run(fmax=0.01)
write('n5_opt_rigid.cube', n5)
