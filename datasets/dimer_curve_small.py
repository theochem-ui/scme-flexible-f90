import time

import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.visualize import view
from ase.constraints import FixBondLengths, FixAtoms
from ase.optimize import BFGS


# NEW ?
te=1.13922136e+00  
td=8.50782754e+00  
Ar=7.70884702e+03 
br=-5.36242472e-01
cr=-3.48164066e+00

te=1.09474041e+00  
td=7.60024435e+00  
Ar=8.62564084e+03 
br=-5.17153235e-01
cr=-3.50240450e+00

# Better dimer curve (less repulsive closer to CCSD) - BEST SO FAR!!!
#te=1.10454150e+00  
#td=7.55480010e+00  
#Ar=8.14963246e+03 
#br=-5.51527551e-01
#cr=-3.46949797e+00

# Dimer curve-full, trimer, quad, penta, hexa and bulk objective
#te=1.10436585e+00  
#td=5.14116094e+00  
#Ar=1.07220263e+04 
#br=-8.97152811e-01
#cr=-3.46046223e+00

# Dimer trimer op set
#te=1.17622083e+00  
#td=4.47276409e+00  
#Ar=2.20757402e+04 
#br=-1.28571582e+00
#cr=-4.00000000e+00

# Dimer only - repulsion at short  not strong enough for current model
#te=1.09827566e+00  
#td=1.10000000e+01  
#Ar=3.67144056e+03 
#br=-5.00000000e-02
#cr=-3.36850296e+00

# Dimer, trimer, quad, penta, hexa
#te=1.20363264e+00                                                                           
#td=5.36055971e+00  
#Ar=9.63937695e+03 
#br=-7.18544048e-01
#cr=-3.55369543e+00

T1 = time.time()

data = open('dimer_curve.data', 'w')

n2 = read('scme_dimer.xyz')
n2.cell = (35,35,35)
n2.center()

# Move H2O B to first x-position
pos = n2.positions.copy()
pos[3:,0] -= 0.175

n2.set_positions(pos)

d = np.zeros(32)
e_b = np.zeros(32)

# Part 1: 
for a in range(32):
    dis = n2.get_distance(0,3)
    d[a] = dis
    # Set constraints
    f = []
    for i in range(len(n2)//3):
        f.append([i*3,i*3+1])
        f.append([i*3,i*3+2])
        f.append([i*3+1,i*3+2])
     
    c = FixBondLengths(f)
    b = FixAtoms(indices=[atom.index for atom in n2 if atom.symbol == 'O'])
    n2.set_constraint(b)
    # Optimize
    n2.calc = SCME_PS(n2, 
                      numerical=False,
                      te=te,
                      td=td,
                      AO=Ar,
                      bO=br,
                      cO=cr)

    dyn = BFGS(n2, trajectory='dimer/log.traj')
    dyn.run(fmax=0.01)
    write('dimer/water_dimer_%d_target.cube' %a, n2)

    E = n2.get_potential_energy()
    E -= 0.018
    e_b[a] = E

    data.write('%s   %s\n' %(dis, E))
    # Move
    n2.constraints = []
    pos = n2.positions.copy()
    # Move A and B by 0.0125 along x-axis
    pos[:3,0] -= 0.006
    pos[3:,0] += 0.006
    n2.set_positions(pos)

print(time.time()-T1)

def adjust(energy, distance, index, de):
    # index marks zero : 23
    a = de / abs(distance[index-2] - distance[index])
    energy += a * (distance - distance[index])
 
    return energy

#e_b = adjust(e_b, d, 23, 0.004)

def shift(energy, dE):
    return energy+dE

#e_b = shift(e_b, -0.013)

import pylab as pl
pl.plot(d, e_b, 'r--')
pl.show()

