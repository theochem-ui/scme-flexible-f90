import time

import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.visualize import view
from ase.constraints import FixBondLengths, FixAtoms
from ase.optimize import BFGS

te=1.10036499e+00
td=7.09887127e+00
Ar=8.66033865e+03
br=-5.64462066e-01
cr=-3.48562837e+00

T1 = time.time()

data_3B = open('scan_3B.data', 'w')
data_2B = open('scan_2B.data', 'w')
#
trimers = read('scan.xyz', index=':')

from ase.units import kcal, mol

e_all = []

for a in trimers:

    a.center(vacuum=25)

    a.calc = SCME_PS(a,
                      numerical=False,
                      te=te,
                      td=td,
                      AO=Ar,
                      bO=br,
                      cO=cr)
    e = a.get_potential_energy()

    e *= mol / kcal

    data_3B.write('%s\n' %e)

    # Run over all possible pairs and calculate 2B
    b = a[:6]
    c = a[:3] + a[6:]
    d = a[3:]

    b.calc = SCME_PS(b,                          
                      numerical=False,            
                      te=te,                      
                      td=td,                      
                      AO=Ar,                      
                      bO=br,                      
                      cO=cr)                      
    eb = b.get_potential_energy()

    c.calc = SCME_PS(c,                          
                      numerical=False,            
                      te=te,                      
                      td=td,                      
                      AO=Ar,                      
                      bO=br,                      
                      cO=cr)                      
    ec = c.get_potential_energy()

    d.calc = SCME_PS(d,                          
                      numerical=False,            
                      te=te,                      
                      td=td,                      
                      AO=Ar,                      
                      bO=br,                      
                      cO=cr)                      
    ed = d.get_potential_energy()

    e2B = (eb + ec + ed) * mol / kcal
    data_2B.write('%s\n' %e2B)

