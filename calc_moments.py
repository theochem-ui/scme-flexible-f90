from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
import numpy as np

traj = Tr('h2o.traj')
h2o = traj[-1]

# read in angles and distances
geos = open('../make_water/water_geometry_total.data', 'r')
lines = geos.readlines()

# output
dips  = open('scme_dipole_total.data', 'w')
quads = open('scme_quads_total.data', 'w')

#[0.28150792 0.75139556]

pos = h2o.positions.copy()

#0.77537908  0.93080656 -0.93207121 -1.50352002  0.24685927 -0.25005065
#  0.90558297]

for line in lines:
    h2o = traj[-1]
    ang = float(line.split()[0])
    r1  = float(line.split()[1])
    r2  = float(line.split()[2])

    # Add dummy atom at -z below oxygen
    O = h2o[0:1].copy()
    O.positions[0,2] -= 1.0

    h2oD = h2o + O

    h2oD.set_angle(3,0,1,ang/2.)
    h2oD.set_angle(3,0,2,ang/2.)

    h2oD.set_distance(0,1,r1,fix=0)
    h2oD.set_distance(0,2,r2,fix=0)

    del h2oD[3]

    dR = (pos - h2oD.positions).flatten()
    rms = np.sqrt((dR**2).sum() / len(dR))

    from ase_interface_mpi import SCME_PS

    h2oD.calc = SCME_PS(h2oD,
                   numerical=True,
                   fix_oxygen=False,
                   parallel=True,
                   static_flag='dqoh',
                   ind_flag='dqc',
                   flex_flag='dq',
                   )
    h2oD.get_potential_energy()

    dip  = h2oD.calc.dip[0]
    quad = h2oD.calc.quad[0]
    print(dip)

    dips.write('%s  %s  %s  %s  %s  %s  %s\n' 
                %(str(ang), str(r1), str(r2), str(dip[0]), str(dip[1]), str(dip[2]), str(rms)))

    quads.write('%s  %s  %s  %s  %s  %s  %s  %s  %s  %s\n' 
                %(str(ang), str(r1), str(r2), str(quad[0,0]), 
                  str(quad[1,1]), str(quad[2,2]), str(quad[0,1]),
                  str(quad[0,2]), str(quad[1,2]), str(rms)))
dips.close()
quads.close()
