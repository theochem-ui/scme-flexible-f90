! Copyright (c)  2015-2016  SCMEdev.
! Licenced under the LGPLv3 license. See LICENSE for details.


!> Module for calculating energy and forces of water moecules using the SCME
!! potential that is based on multipole moments, where ach molecule is
!! represented as a multipole expansion up to hexadecapole moment.
!!
!! The SCME potential is described in:
!!
!!      K. T. Wikfeldt, E. R. Batista, F. D. Vila and H. Jonsson
!!      Phys. Chem. Chem. Phys., 2013, 15, 16542
!!
!! Please cite this work if you use the SCME potential in you research.
module scme

  use data_types
  use max_parameters
  use parameters, only: num_cells
  use multipole_parameters, only: d0, q0, o0, h0
  use polariz_parameters, only: dd0, dq0, hp0, qq0
  use molecProperties, only: recoverMolecules, calcCentersOfMass,bisectorAxes,&
       findPpalAxes, rotatePoles, rotatePolariz, setUnpolPoles, addFields, &
       addDfields, atomicForces, atomicFFlexible, dummyAtoms
  use calc_lower_order, only: calcEdip_quad
  use calc_higher_order, only: calcEhigh
  use calc_derivs, only: calcDv
  use inducePoles, only: induceDipole, induceQpole
  use forceCM_mod, only: forceCM
  use torqueCM_mod, only: torqueCM
  !use atomicForces_mod, only: atomicForces
  use calcEnergy_mod, only: calcEnergy
  use coreInt_mod, only: coreInt
  use dispersion_mod, only: dispersion

  implicit none
  private
  public scme_calculate
  public HAS

contains

  !> The main routine for the SCME potential. Calculates the total energy and
  !! forces on a set of water molecules.
  !!
  !! @param[in] n_atoms : The number of atoms. The number of atoms is assumed
  !!                      to be 3 times the number of water molecules.
  !! @param[in] coords  : The coordinates of the water molecules.
  !!                  coords(l+6*(i-1)) stores the l-th coordinate of the first
  !!                      hydrogen in the i-th molecule
  !!                  coords(l+3+6*(i-1)) stores the l-th coordinate of the second
  !!                      hydrogen in the i-th molecule
  !!                  coords(l+3*(i-1+nHydrogens)) stores the l-th coordinate of the
  !!                      oxygen in the i-th molecule. (nHydrogens is
  !!                      the total number of hydrogen atoms).
  !! @param[in] lattice : The x,y,z dimensions of the rectangular box.
  !! @param[out] fa     : The forces. The array must have space for n_atoms forces.
  !! @param[out] u_tot  : The total energy calculated with the SCME potential.
  subroutine scme_calculate(te, td, Ar, br, cr, n_atoms, &
                  & coords, lattice, static_flag, &
                  & ind_flag, flex_flag, & 
                  & fa, u_tot, fCM, dpole, qpole)

    implicit none
    integer, intent(in) :: n_atoms
    double precision, intent(in) :: coords(n_atoms*3)
    double precision, intent(in) :: lattice(3)
    character(len=4), intent(in) :: static_flag
    character(len=3), intent(in) :: ind_flag
    character(len=2), intent(in) :: flex_flag
    double precision, intent(out) :: fa(n_atoms*3)
    double precision, intent(out) :: u_tot
    ! ----------------------------------------
! TEST
integer :: ii,jj,kk
    ! Constants and parameters.
    real(dp), parameter :: pi = 3.14159265358979324d0
    real(dp), parameter :: kk1 = 2.5417464506241085D0
    real(dp), parameter :: kk2 = 1.8897261258369282D0
    real(dp), parameter :: convFactor = 14.399645469227202D0 / 4.80320467299766D0**2
    real(dp), parameter :: rMax = 11.d0
    !! real(dp), parameter :: rMax = 7.0d0
    real(dp), parameter :: rMax2 = rMax*rMax
    integer, parameter :: NC = num_cells

    ! Parameters for optimization
    real(dp), parameter :: fact_a =  0.78873185d0
    real(dp), parameter :: fact_b =  0.80112091d0
    real(dp), parameter :: fact_c = -0.59143448d0
    real(dp), parameter :: f_req  = -1.27942452d0
    real(dp), parameter :: t_req  =  0.46940746d0
    real(dp), parameter :: f_ang  = -0.13744214d0
    real(dp), parameter :: t_ang  =  1.0027335d0
    double precision, intent(in) :: te
    double precision, intent(in) :: td
    double precision, intent(in) :: Ar
    double precision, intent(in) :: cr
    double precision, intent(in) :: br

    ! Parameter flags for controlling behavior.
    logical*1, parameter :: irigidmolecules = .false.
    logical*1, parameter :: debug = .false.
    logical*1, parameter :: iSlab = .false.
    logical*1, parameter :: addCore = .true.
    logical*1, parameter :: useDMS = .true.
    logical*1, parameter :: hotS = .false.

    ! Local flag for controlling behavior.
    logical*1 :: converged

    ! Local variables for energies.
    real(dp) :: uQ, uH, uES, uDisp, uD, uCore

    ! Local arrays for lattice and half lattice.
    real(dp) :: a(3), a2(3)

    ! Center of mass forces and torque.
    double precision, intent(out) :: fCM(3,n_atoms/3)
    real(dp) :: fsf(3,n_atoms/3), tau(3,n_atoms/3)

    ! Atomic positions, centers of mass
    real(dp) :: ra(n_atoms*3), rCM(3,n_atoms/3)

    ! Rotation matrix - global to local. Derivative of rot matrix
    real(dp) :: x(3,3,n_atoms/3)
    real(dp) :: dx(3,3,3,n_atoms/3), dxx(3,3,3,n_atoms/3), &
                dxz(3,3,3,n_atoms/3)

    ! Electric fields.
    real(dp) :: eD(3,n_atoms/3), eQ(3,n_atoms/3), &
                eH(3,n_atoms/3), eT(3,n_atoms/3)

    real(dp) :: eDa(3,n_atoms), eQa(3,n_atoms), &
                eHa(3,n_atoms), eTa(3,n_atoms)

    ! Derivatives of E.
    real(dp) :: dEddr(3,3,n_atoms/3), dEqdr(3,3,n_atoms/3), &
                dEhdr(3,3,n_atoms/3), dEtdr(3,3,n_atoms/3)

    real(dp) :: dEddra(3,3,n_atoms), dEqdra(3,3,n_atoms), &
                dEhdra(3,3,n_atoms), dEtdra(3,3,n_atoms)

    ! High order derivatives of the potential.
    real(dp) :: d1v(3,n_atoms/3)
    real(dp) :: d2v(3,3,n_atoms/3)
    real(dp) :: d3v(3,3,3,n_atoms/3)
    real(dp) :: d4v(3,3,3,3,n_atoms/3)
    real(dp) :: d5v(3,3,3,3,3,n_atoms/3)

    ! Work multipoles. They start unpolarized and with the induction
    ! loop we induce dipoles and quadrupoles.
    real(dp) :: dpole0(3,n_atoms/3)
    real(dp) :: qpole0(3,3,n_atoms/3)
    real(dp) :: opole(3,3,3,n_atoms/3)
    real(dp) :: hpole(3,3,3,3,n_atoms/3)
    !real(dp) :: dpole(3,n_atoms/3)
    !real(dp) :: qpole(3,3,n_atoms/3)

    double precision, intent(out) :: dpole(3,n_atoms/3)
    double precision, intent(out) :: qpole(3,3,n_atoms/3)

    ! Polarizabilities.
    real(dp) :: dd(3,3,n_atoms/3)
    real(dp) :: dq(3,3,3,n_atoms/3)
    real(dp) :: hp(3,3,3,n_atoms/3)
    real(dp) :: qq(3,3,3,3,n_atoms/3)

    ! Local integers.
    integer :: nM, nO, nH, i, p, k, l, ll
    integer :: indO, indH1, indH2

    ! Variable static
    real(dp) :: d0a(3)
    real(dp) :: q0a(3,3)
    real(dp) :: o0a(3,3,3)
    real(dp) :: h0a(3,3,3,3)
    real(dp) :: dd0a(3,3)
    real(dp) :: dq0a(3,3,3)
    real(dp) :: qq0a(3,3,3,3)

    ! Local arrays for ??? ML
    real(dp) :: uPES(n_atoms*3)
    real(dp) :: qdms(3)
    real(dp) :: dqdms(3,3,3,n_atoms/3)
    real(dp) :: dipmom(3)
    real(dp) :: quadmom(3,3)
    real(dp) :: quadmomeq(3,3)
    real(dp) :: octomom(3,3,3)
    real(dp) :: hexamom(3,3,3,3)
    real(dp) :: dr_O(3)
    real(dp) :: dr_H1(3)
    real(dp) :: dr_H2(3)
    real(dp) :: r_O, r_H1, r_H2
    real(dp) :: qeq(3)
    real(dp) :: qen(3)
    real(dp) :: y_gamma
    real(dp) :: qedip(3)
    real(dp) :: qatoms(n_atoms)
    real(dp) :: d1(3,n_atoms/3)
    real(dp) :: d2(3,n_atoms/3)
    real(dp) :: dd1(3,3,3,n_atoms/3) ! gamma,alpha,atom(H1,H2,O),molecule
    real(dp) :: dd2(3,3,3,n_atoms/3)
    real(dp) :: dr_d1(3)
    real(dp) :: dr_d2(3)
    real(dp) :: r_d1
    real(dp) :: r_d2

    ! Input for the potnasa potential.
    real(dp) :: mol(9)
    real(dp) :: grad(9)
    real(dp) :: uPES1
    real(dp) :: qO0
    real(dp) :: qH0
    ! ----------------------------------------

! miscellaneous
integer :: counter


    ! Total energy.
    u_tot = 0.0d0
    qatoms(:) = 0.d0

    ! Number of oxygen, hydrogen and molecules.
    nO = n_atoms/3
    nH = nO*2
    nM = nO

    ! For flexible dipole/quadrupole
    ! y_gamma = 0.40716616d0 ! Places M-site such that quad = Q_T
    y_gamma = 0.d0

    ! Size of the simulation cell.
    a(1) = lattice(1)
    a(2) = lattice(2)
    a(3) = lattice(3)
    a2(1) = lattice(1)/2.0d0
    a2(2) = lattice(2)/2.0d0
    a2(3) = lattice(3)/2.0d0

    ! Set unrotated
    d0a(:) = d0(:)
    q0a(:,:) = q0(:,:)
    dd0a(:,:) = dd0(:,:)
    o0a(:,:,:) = o0(:,:,:)
    dq0a(:,:,:) = dq0(:,:,:)
    h0a(:,:,:,:) = h0(:,:,:,:)
    qq0a(:,:,:,:) = qq0(:,:,:,:)

   !  do i  = 1,nM*9

   !        print* , "coords", i, ":",coords(i)
   !    end do 
!  do i  = 1,3

!           print* , "a", i, ":",a(i)
!           print* , "a2", i, ":",a2(i)

!       end do 
    ! Recover broken molecules due to periodic boundary conditions.
    call recoverMolecules(coords, ra, nH, nO, a, a2)
   !  print*, "scme recover"
   !  do i  = 1,nM*9

   !        print* , "coords", i, ":",coords(i)
   !    end do 
   !  do i  = 1,nM*9

   !        print* , "ra", i, ":",ra(i)
   !    end do 
   !  print* , "coords", coords
   !  print* , "ra" ,  ra
    ! Calculate the center of mass for each molecule.
    call calcCentersOfMass(ra, nM, rCM)

!     print*, "rCM"
! print*, rCM
! print*, "rCM"
    ! Find the rotation matrix x from local to global reference frame and derivatives
    call bisectorAxes(ra, nM, x, dx, dxx, dxz, rCM)



   !  do ii = 1,nM
   !  do jj = 1,3
   !  do kk = 1,3
   ! !  do ll = 1,3
   !  print*,"x", ii, jj, kk, x(jj,kk,ii)
   ! !  end do
   !  end do
   !  end do
   !  end do


   !  do ii = 1,nM
   !  do jj = 1,3
   !  do kk = 1,3
   !  do ll = 1,3
   !  print*,"dx", ii, jj, kk, ll, dx(jj,kk,ll,ii)
   !  end do
   !  end do
   !  end do
   !  end do


!     do ii = 1,nM
!     do jj = 1,3
!     do kk = 1,3

!     print*,"x", ii, jj, kk,  x(jj,kk,ii)
! end do 
! end do
! end do
    ! Rotate the multipoles d0, dq etc, to allign with the molecules using
    ! the rotation matrix x. The result is stored in the dpole0 etc arrays.
    call rotatePoles(d0, q0, o0, h0, dpole0, qpole0, opole, hpole, nM, x)

!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"qpole0", ii, jj, kk,  qpole0(jj,kk,ii)
! end do 
! end do
! end do

!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"qpole0", ii, jj, kk,  qpole0(jj,kk,ii)
! end do 
! end do
! end do


    ! call Partridge-Schwenke dipole moment surface routine.
    if (useDMS) then

       qO0 = -0.66194176905d0
       qH0 = 0.330970884525d0

       do i = 1,nM
          indH1 = 6*(i-1)
          indO  = 3*(i-1+2*nM)
          indH2 = 3+6*(i-1)

          ! Get x, y, z coordinates for H and O.
          do p=1,3
             mol(p)   = ra(indO  + p)
             mol(p+3) = ra(indH1  + p)
             mol(p+6) = ra(indH2  + p)
             dr_O(p)  = mol(p)   - rCM(p,i)
             dr_H1(p) = mol(p+3) - rCM(p,i)
             dr_H2(p) = mol(p+6) - rCM(p,i)
          end do

            ! print*, mol
            ! print*, dr_O 
            ! print*, dr_H1 
            ! print*, dr_H2

!     do ii = 1,3
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"dxx", ii, jj, kk,   dxz(ii,jj,kk,i)
! end do 
! end do
! end do

          call dummyAtoms(mol,rCM(:,i),x(:,:,i),d1(:,i),d2(:,i), dd1(:,:,:,i), & 
                          & dd2(:,:,:,i),f_req,t_req,f_ang,t_ang,dx(:,:,:,i), &
                          & dxx(:,:,:,i), dxz(:,:,:,i))
!     do ii = 1,3
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"dd2", ii, jj, kk,   dd2(ii,jj,kk,i)
! end do 
! end do
! end do
        
          call dmsnasa2(mol,qdms,dqdms(:,:,:,i))

!               do ii = 1,3


!     print*,"qdms", ii, qdms(ii)

! end do
!     do ii = 1,3
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"dqdms",i, ii, jj, kk,   dqdms(ii,jj,kk,i)
! end do 
! end do
! end do
          do p = 1, 3
             dr_d1(p) = d1(p,i) !- rCM(p,i)
             dr_d2(p) = d2(p,i) !- rCM(p,i)
          end do

          qen(1) = fact_a * qdms(1) + fact_b * qO0
          qen(2) = fact_a * qdms(2) + fact_b * qH0
          qen(3) = fact_a * qdms(3) + fact_b * qH0

          ! Hold on to partial charge for force calculation
          qatoms((i-1)*3+1) = qdms(1)
          qatoms((i-1)*3+2) = qdms(2)
          qatoms((i-1)*3+3) = qdms(3)

          r_O  = dr_O(1)**2 + dr_O(2)**2 + dr_O(3)**2
          r_H1 = dr_H1(1)**2 + dr_H1(2)**2 + dr_H1(3)**2
          r_H2 = dr_H2(1)**2 + dr_H2(2)**2 + dr_H2(3)**2
          r_d1 = dr_d1(1)**2 + dr_d1(2)**2 + dr_d1(3)**2
          r_d2 = dr_d2(1)**2 + dr_d2(2)**2 + dr_d2(3)**2
! print*, "r_O  : ", r_O 
! print*, "r_H1 : ", r_H1
! print*, "r_H2 : ", r_H2
! print*, "r_d1 : ", r_d1
! print*, "r_d2 : ", r_d2
! print*, "qen(1):" , qen(1)
! print*, "qen(2):" , qen(2)
! print*, "qen(3):" , qen(3)
          ! Calculate dipole moment wrt center of mass.
          dipmom(:) = 0.0d0
          quadmomeq(:,:) = 0.d0
          do p=1,3
             dipmom(p) = dipmom(p) + qdms(1)*dr_O(p)
             dipmom(p) = dipmom(p) + qdms(2)*dr_H1(p)
             dipmom(p) = dipmom(p) + qdms(3)*dr_H2(p)
             do k=1,3
                quadmomeq(p,k) = quadmomeq(p,k) + 3.0d0*qen(2)*dr_H1(p)*dr_H1(k)*0.5d0
                quadmomeq(p,k) = quadmomeq(p,k) + 3.0d0*qen(3)*dr_H2(p)*dr_H2(k)*0.5d0
                quadmomeq(p,k) = quadmomeq(p,k) + 3.0d0*fact_c*qen(2)*dr_d1(p)*dr_d1(k)*0.5d0
                quadmomeq(p,k) = quadmomeq(p,k) + 3.0d0*fact_c*qen(3)*dr_d2(p)*dr_d2(k)*0.5d0
               !   print*, "quadmomeq", p, k,  quadmomeq(p,k)
                if (p==k) then
                    quadmomeq(p,k) = quadmomeq(p,k) - qen(2)*r_H1*0.5d0
                    quadmomeq(p,k) = quadmomeq(p,k) - qen(3)*r_H2*0.5d0
                    quadmomeq(p,k) = quadmomeq(p,k) - fact_c*qen(2)*r_d1*0.5d0
                    quadmomeq(p,k) = quadmomeq(p,k) - fact_c*qen(3)*r_d2*0.5d0
               !   print*, "quadmomeq", p, k,  quadmomeq(p,k)

                end if
             end do
          end do
         !  do p=1,3
         !  do k=1,3
         !    print*, "quadmomeq", p, k,  quadmomeq(p,k)
         ! end do
         ! end do
          ! Set unpolarized dipoles to Partridge-Schwenke dipoles
          ! using conversion constants for eA -> D.
          do p=1,3

             if (HAS(flex_flag,'d')) then
                dpole0(p,i) = dipmom(p) * kk1 * kk2
                d0a(:) = 0.d0
             end if

             if (HAS(flex_flag,'q')) then
                do k=1,3
                   qpole0(p,k,i) = quadmomeq(p,k) * kk1 * kk2
                   q0a(:,:) = 0.d0
                end do
             end if
          end do
       end do
    end if


!     do ii = 1,nM
!     do jj = 1,3
!     print*,"dpole0", ii, jj, dpole0(jj,ii)
! !     do kk = 1,3

! !     print*,"qpole0", ii, jj, kk,  qpole0(jj,kk,ii)
! ! end do 
! end do
! end do


!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"qpole0", ii, jj, kk,  qpole0(jj,kk,ii)
! end do 
! end do
! end do
    ! MORE FLAG shenanigans for induced and static moments
    if (.not. HAS(static_flag,'d')) then
       dpole0 = 0.0d0
       d0a = 0.d0
    end if

    if (.not. HAS(static_flag, 'q')) then
       qpole0 = 0.0d0
       q0a = 0.d0
    end if

    if (.not. HAS(static_flag, 'o')) then
       opole = 0.0d0
       o0a = 0.d0
    end if

    if (.not. HAS(static_flag, 'h')) then
       hpole = 0.d0
       h0a   = 0.d0
    end if

    ! NEEDS DOCUMENTATION
    call setUnpolPoles(dpole, qpole, dpole0, qpole0, nM)


   
!     do ii = 1,nM
!     do jj = 1,3
!     print*,"dpole", ii, jj, dpole(jj,ii)
! !     do kk = 1,3

! !     print*,"qpole0", ii, jj, kk,  qpole(jj,kk,ii)
! ! end do 
! end do
! end do

   
!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"dpole0", ii, jj, dpole0(jj,ii)
!     do kk = 1,3

!     print*,"qpole", ii, jj, kk,  qpole(jj,kk,ii)
! end do 
! end do
! end do

    call rotatePolariz(dd0, dq0, qq0, hp0, dd, dq, qq, hp, nM, x)
      
!     do jj = 1,3
!    !  print*,"eH", ii, jj, eH(jj,ii)
!     do kk = 1,3

!     print*,"dd0", ii, jj, kk,  dd0(jj,kk)
! end do 
! end do

!         do ii = 1,nM
!     do jj = 1,3
!    !  print*,"eH", ii, jj, eH(jj,ii)
!     do kk = 1,3

!     print*,"dd", ii, jj, kk,  dd(jj,kk,ii)
! end do 
! end do
! end do
    call calcEhigh(rCM, opole, hpole, nM, NC, a, a2, uH, eH, dEhdr, rMax2, iSlab, te)
!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"eH", ii, jj, eH(jj,ii)
!     do kk = 1,3

!     print*,"dEhdr", ii, jj, kk,  dEhdr(jj,kk,ii)
! end do 
! end do
! end do
    if (.not.HAS(ind_flag, 'd')) then
       dd = 0.0d0
       dd0a = 0.d0
    end if

    if (.not.HAS(ind_flag, 'q')) then
       qq = 0.0d0
       qq0a = 0.d0
    end if

    if (.not.HAS(ind_flag, 'c')) then
       dq = 0.0d0
       dq0a = 0.d0
    end if

    ! Here's where the induction loop begins.
    converged = .false.

    counter = 0.0
    do while (.not. converged)

       ! NEEDS DOCUMENTATION
       call calcEdip_quad(rCM, dpole, qpole, nM, NC, a, a2, uD, uQ, eD, dEddr, rMax2, iSlab, te)
!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"eH", ii, jj, eD(jj,ii)
!     do kk = 1,3

!     print*,"dEhdr", ii, jj, kk,  dEhdr(jj,kk,ii)
! end do 
! end do
! end do
! print*, "-----------"

       call addFields(eH, eD, eT, nM)
       call addDfields(dEhdr, dEddr, dEtdr, nM)
!     do ii = 1,nM
!     do jj = 1,3
!    !  print*,"eH", ii, jj, eH(jj,ii)
!    !  print*,"eD", ii, jj, eD(jj,ii)
!    !  print*,"eT", ii, jj, eT(jj,ii)
!     do kk = 1,3

!     print*,"dEtdr", ii, jj, kk,  dEtdr(jj,kk,ii)
! end do 
! end do
! end do
       ! Induce dipoles and quadrupoles.
       converged = .true.

!                   do ii = 1,nM
!     do jj = 1,3
!     print*,"dpole1", ii, jj, dpole0(jj,ii)
! !     do kk = 1,3

! !     print*,"dd1", ii, jj, kk,   dd1(ii,jj,kk,i)
! ! end do 
! end do
! end do
       call induceDipole(dpole, dpole0, eT, dEtdr, dd, dq, nM, converged)
!            do ii = 1,nM
!     do jj = 1,3
!     print*,"dpole2", ii, jj, dpole(jj,ii)
! !     do kk = 1,3

! !     print*,"dd1", ii, jj, kk,   dd1(ii,jj,kk,i)
! ! end do 
! end do
! end do
       call induceQpole(qpole, qpole0, eT, dEtdr, dq, qq, nM, converged)
      counter =  counter + 1.0
    end do

print*, "counter :", counter

!     do ii = 1,nM
!     do jj = 1,3
!     print*,"dpole", ii, jj, dpole(jj,ii)
! !     do kk = 1,3

! !     print*,"dd1", ii, jj, kk,   dd1(ii,jj,kk,i)
! ! end do 
! end do
! end do

    ! With the polarized multipoles, calculate the derivarives of the
    ! electrostatic potential, up to 5th order.
    call calcDv(rCM, dpole, qpole, opole, hpole, nM, NC, a, a2, &
            d1v, d2v, d3v, d4v, d5v, rMax2, fsf, iSlab, te)
!     do ii = 1,nM
!     do jj = 1,3
!     print*,"d1v", ii, jj, d1v(jj,ii)
!    !  do kk = 1,3

!    !  print*,"dEhdr", ii, jj, kk,  dEhdr(jj,kk,ii)
! ! end do 
! end do
! end do
    call calcEnergy(dpole0, qpole0, opole, hpole, d1v, d2v, d3v, d4v, nM, u_tot)
    !
    call forceCM(dpole, qpole, opole, hpole, d2v, d3v, d4v, d5v, nM, fsf, fCM)

!     do ii = 1,nM
!     do jj = 1,3
!     print*,"fCM", ii, jj, fCM(jj,ii)
! !     do kk = 1,3

! !     print*,"dd1", ii, jj, kk,   dd1(ii,jj,kk,i)
! ! end do 
! end do
! end do


    ! Only used for checks against the atomic forces
    call torqueCM(dpole, qpole, opole, hpole, d1v, d2v, d3v, d4v, nM, tau)

    ! Calculate atomic forces using local frame moments, potential fields and 
    ! derivatives of rotation matrices.

!    !  do ii = 1,nM
!     do jj = 1,3
!     print*,"d0a", jj, d0a(jj)
! !     do kk = 1,3

! !     print*,"dd1", ii, jj, kk,   dd1(ii,jj,kk,i)
! ! end do 
! end do
! ! end do


   !  do ii = 1,3
   !  do jj = 1,3
   !  print*,"q0a", ii, jj, q0a(ii,jj)
   !  end do
   !  end do



   !  do ii = 1,nM
   !  do jj = 1,3
   !  do kk = 1,3
   !  do ll = 1,3
   !  print*,"dx", ii, jj, kk, ll, dx(jj,kk,ll,ii)
   !  end do
   !  end do
   !  end do
   !  end do

    call atomicForces(d0a, q0a, o0a, h0a, dd0a, dq0a, qq0a, d1v, d2v, d3v, d4v, &
            x, dx, dxx, dxz, fa, fCM, convFactor, nM)
   !   do ii = 1,nM*9
   !      print*, "atomic forces", ii, ":", fa(ii)
   !    end do
    ! Some nonsense due to flexible molecule
    if (.not.HAS(flex_flag,'d')) then
       d1v(:,:) = 0.d0
    end if

    if (.not.HAS(flex_flag,'q')) then
       d2v(:,:,:) = 0.d0
    end if

    ! Calculate atomic forces due to flexible moments
    if (useDMS) then
       call atomicFFlexible(qatoms, d1v, d2v, ra, rCM, fa, convFactor, kk1, kk2, nM, &
               dqdms, qO0, qH0, fact_a, fact_b, fact_c, d1, d2, dd1, dd2)
    end if
   !   do ii = 1,nM*9
   !      print*, "atomic forces", ii, ":", fa(ii)
   !    end do
    u_tot = u_tot * convFactor

    ! u_tot = 0.d0
    ! Store the energy this far for debug printout.
    uES = u_tot

    print*, "uES :", uES 

    ! Calculate dispersion forces ??? ML
    call dispersion(ra, fa, uDisp, nM, a, a2, td)
    u_tot = u_tot + uDisp

    print*, "uDisp :", uDisp

   !   do ii = 1,nM*9
   !      print*, "atomic forces", ii, ":", fa(ii)
   !    end do
    ! Calculate the core contribution to the energy. (only to the energy ??? ML)
    if (addCore) then
       call coreInt(ra, fa, uCore, nM, a, a2, Ar, br, cr)
       u_tot = u_tot + uCore
    end if

    print*, "uCore :", uCore

   !  do ii = 1,nM*9
   !      print*, "atomic forces", ii, ":", fa(ii)
   !    end do

    ! Adding intramolecular energy from Partridge-Schwenke PES.
    uPES(:) = 0.0d0
    if (.not. irigidmolecules) then
       do i=1,nM
          mol(:) = 0.0d0
          grad(:) = 0.0d0

          indH1 = 6*(i-1)
          indO  = 3*(i-1+2*nM)
          indH2 = 3+6*(i-1)
          ! Get x, y, z coordinates for H and O.
          do p=1,3
             mol(p) = ra(indO  + p)
             mol(p+3) = ra(indH1  + p)
             mol(p+6) = ra(indH2  + p)
          end do

          call potnasa2(mol,grad,uPES1)
          uPES(i) = uPES1
          u_tot = u_tot + uPES1
          do p=1,3
             fa(indO  + p) = fa(indO + p) - grad(p)
             fa(indH1 + p) = fa(indH1 + p) - grad(p+3)
             fa(indH2 + p) = fa(indH2 + p) - grad(p+6)
          end do
       end do
    end if
     do ii = 1,nM*9
        print*, ii, ":", fa(ii)
      end do
      print*, "u_tot : ", u_tot
    return

  end subroutine scme_calculate

  function HAS(A,B) !Text B appears somewhere in text A?
   character*(*) A,B
   integer l
   logical*1 HAS

    l = index(A,B)
    if (l.le.0) then
      HAS = .false.
     else
      HAS = .true.
    end if
  end function

end module scme

