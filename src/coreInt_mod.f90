module coreInt_mod
  
  use data_types
  use max_parameters
  use parameters, only: coreInt_c1, coreInt_c2, coreInt_c3, coreInt_c4
  
  use rho, only: calcRho, calcAmp, dDens
  
  implicit none
  
  private
  public coreInt
  
contains
  
  
  subroutine coreInt(ra, fa, uCore, nM, a, a2, Ar, br, cr)
    
    implicit none
    
    integer nM, ai(10), kk, jj
    real(dp) t1, t2, df, uCore, r, a(3), a2(3), avg
    real(dp) ra(maxCoo), fa(maxCoo), c1, c2, c3, c4, c5,t11, t12, t13
    real(dp) Amp(maxCoo/3), dAmp(maxCoo/3), rho(maxCoo/3), f, rMax2
    real(dp) Ri1(3), Ri(3), Ri2(3), Rj1(3), Rj(3), Rj2(3)
    real(dp) vRi1j1(3), vRi2j2(3), vRij(3)
    real(dp) Rij
    real(dp) S
    real(dp) uR_An
    real(dp) an_1, an_2, an_3, an_4
    real(dp) df1, df2
    integer*4 Pt_i, Pt_i1, Pt_i2
    integer*4 Pt_j, Pt_j1, Pt_j2
    real(dp) Ar, br, cr 
    integer i, j, k, n, m, iOn, iOm, iOi, iOj, p
    real(dp) dr(3)
    
    c1 = coreInt_c1
    c2 = coreInt_c2
    c3 = coreInt_c3
    c4 = coreInt_c4
    
    uCore = 0.0D0
    
    do n = 1, nM-1
       
       ! Get the index of the first O atom
       iOn = 3 * (2*nM + n - 1)
       
       do m = n+1, nM
          
          ! Get the index of the second O atom
          iOm = 3 * (2*nM + m - 1)
          
          ! Adjust the O-O distance for the PBC's
          do i = 1, 3
             dr(i) = ra(iOm+i) - ra(iOn+i)
             if     (dr(i) .gt. a2(i)) then
                dr(i) = dr(i) - a(i)
             elseif (dr(i) .lt. -a2(i)) then
                dr(i) = dr(i) + a(i)
             end if
          end do
          
          r = dsqrt(dr(1)**2 + dr(2)**2 + dr(3)**2)
          ! c1 = br, c3 = cr
          t11 = dexp(cr*r)
          !t12 = dexp(c3*r/c4)
          t13 = r**br
          ! c2 = Ar 
          t1 = Ar*t13*t11
          
          uCore = uCore + t1
          
          !t2 = Ar*t13*t11
          
          df = (Ar*(br/r+cr)*t13*t11) / r
          
          do i = 1, 3
             fa(iOn+i) = fa(iOn+i) + df * dr(i)

             print*, "Fa:", iOn+i ,":", fa(iOn+i)
             fa(iOm+i) = fa(iOm+i) - df * dr(i)
          end do
          
       end do
    end do
    
  end subroutine coreInt
  
end module coreInt_mod
