import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
from ase.io.trajectory import Trajectory as Tr
from ase.optimize import BFGS
from ase.visualize import view

a = ['0.928000','0.952000','0.976000',
     '1.072000']

for i, val in enumerate(a):

    # Read in previous ice
    traj = Tr('data/ice-scme-%s.traj' %val)
    ice = traj[-1]
    
    cell = ice.cell.diagonal()
    vol = cell[0]*cell[1]*cell[2]
    
    ice.calc = SCME_PS(ice,
                   numerical=False,
                   static_flag='dqoh', # <--- FLAG to turn on/off static moments
                   ind_flag='dqc',     # <--- FLAG to turn on/off induced moments
                   flex_flag='dq')     # <--- FLAG to turn on/off flexible moments
    
    dyn = BFGS(ice, trajectory='data/ice-scme-%s-new.traj' %val)
    dyn.run(fmax=0.01)
