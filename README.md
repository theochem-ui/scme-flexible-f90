## SCME

To compile the code with f2py: 

make; make scme_f2py -f Makefile.f2py

To test the code:

python3 ttforces.py

When you use the flexible *SCME* potential in your research please **cite** the paper describing the method


    K. T. Wikfeldt, E. R. Batista, F. D. Vila and H. Jonsson
    “A transferable H2O interaction potential based on a single center multipole expansion: SCME”
    Phys. Chem. Chem. Phys., 2013, 15, 16542

    E. Ö. Jónsson, S. Rasti, M. Galynska, J. Meyer and H. Jónsson
    "Transferable Potential Function for Flexible H2O Molecules Based on the Single Center Multipole Expansion"
    arXiv 2020, 10.48550/ARXIV.2007.06090.

