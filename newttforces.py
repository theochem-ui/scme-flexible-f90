import numpy as np
from ase_interface_mpi import SCME_PS
from ase.io import read, write
import ase.units as unit

new = read('test_water.xyz')
new.rotate('z', 45)
n2 = new
#n2 = new[3:] + new[:3]
n2.cell = (25,25,25)
n2.center()

pos = n2.get_positions()

n2.calc = SCME_PS(n2, 
                  numerical=True,
                  static_flag='dqoh',
                  ind_flag='dqc',
                  flex_flag='dq',
                  )


from ase.visualize import view
n2.get_potential_energy()
Fnu = n2.get_forces()

n2.get_potential_energy()

print('-------------Numerical Force------------')
print('mol 1',Fnu[:3])
print('mol 2',Fnu[3:])

n2.calc = SCME_PS(n2, 
                  numerical=False,
                  static_flag='dqoh',
                  ind_flag='dqc',
                  flex_flag='dq',
                  )

print('------------Analytical Force--------------')
Fan = n2.get_forces()
print('mol 1',Fan[:3])
print('mol 2',Fan[3:])

print('------------Root mean square-------------')
print('RMS 1', np.sqrt((1./9. * (Fan[:3] - Fnu[:3])**2).sum()))
print('RMS 2', np.sqrt((1./9. * (Fan[3:] - Fnu[3:])**2).sum()))
print('dF 1', -(Fan[0,0]-Fnu[0,0]), -(Fan[0,1]-Fnu[0,1]), -(Fan[0,2]-Fnu[0,2]))
print('dF 2', -(Fan[3,0]-Fnu[3,0]), -(Fan[3,1]-Fnu[3,1]), -(Fan[3,2]-Fnu[3,2]))
#print(Fan.sum(), Fnu.sum())
print('------------Make dip-quad forces---------')

print('------------Check Num/Ana----------------')
rCM = n2[0].position * 16./18. + n2[1].position/18. + n2[2].position/18.
#print('Num Torque:', n2.calc.tau[0])
A = -(n2[0].position - rCM)
B = -(n2[1].position - rCM)
C = -(n2[2].position - rCM)
tau = -np.cross(A, Fan[0]) - np.cross(B, Fan[1]) - np.cross(C, Fan[2])
print('Ana Torque', tau)
print('Num fCM', n2.calc.fCM[0])
print('Ana fCM', Fan[:3].sum(axis=0))
print('Num fCM', Fnu[:3].sum(axis=0))
print(Fan.sum(), Fnu.sum())
print(Fan - Fnu)
