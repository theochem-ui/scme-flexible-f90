from ase import Atoms
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
import numpy as np
from ase.io import read

from ase_interface_mpi import SCME_PS
from ase.io import write

# Set up water box at 20 deg C density
atoms = read('trimers/UUD.xyz')
atoms.center(vacuum=25)

atoms.set_pbc(True)

te=1.09984359e+00
td=7.61857402e+00
Ar=8.57778344e+03
br=-5.34224485e-01
cr=-3.50375235e+00

class logger:
    def __init__(self, name, atoms):
        self.atoms = atoms
        self.name  = name

    def write(self, atoms=None):
        if atoms != None:
            self.atoms = atoms
        # Check distances: Need two at a time
        for a in [[0,3,0,6],[0,6,3,6],[0,3,3,6]]:
            d1 = self.atoms.get_distance(a[0],a[1])
            d2 = self.atoms.get_distance(a[2],a[3])
            if d1 + d2 > 14.0:
                XXX
        write(self.name+'.xyz', self.atoms, 
              comment=self.atoms.get_potential_energy(), append=True)

T = 300

tag = 'scme_trimer_'+str(T)+'K_F'
atoms.calc = SCME_PS(atoms,
                   te=te,
                   td=td,
                   AO=Ar,
                   bO=br,
                   cO=cr)

md = Langevin(atoms, 0.5 * units.fs, temperature=T * units.kB,
              friction=0.01, logfile=tag + '.log')

#traj = Trajectory(tag + '.traj', 'w', atoms)
log = logger(tag, atoms)
md.attach(log.write, interval=5)
md.run(1000000)
